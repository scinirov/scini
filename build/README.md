## Source Files

These folders contain the full "source" of the SCINI ROV.

**Electrical:** Electrical schematics and PCB layouts for the SCINI ROV.

**Mechanical:** SolidWorks drawings and assemblies of the SCINI ROV and constituent components.

**Source:** Source code for both the command/control and image acquistion of the SCINI ROV.

All materials are released under the Creative Commons BY-SA Licence unless otherwise noted.
