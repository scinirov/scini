##Electronics Repo

These folders contain schematics, PCB files, and in some cases Gerber files for all the SCINI printed circuit boards.

The X-Port board was designed using Eagle Cad which can be downloaded here: www.cadsoftusa.com/download-eagle/freeware/

The other PCBs were designed using Advanced Circuits' PCB Artist, which can be downloaded here: www.4pcb.com/free-pcb-layout-software/

All designs are licenced under the Creative Commons BY-SA licence.
