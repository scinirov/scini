This folder contains the schematic and layout files for the SCINI ROV Controller board. Schematic design and PCB layout was done using PCB Artist, which can be downloaded here: http://www.4pcb.com/free-pcb-layout-software/

Each SCINI ROV contains three of these boards, refered to by their designation in the Atmel source code: CAMERA, MAIN, IMU. 
