#Software Source

This folder contains source code used to build the various software elements of the SCINI ROV. It contains the following elements:


**atmel_mcus:** Source for the ATMega 328 Microcontrollers on-board the SCINI ROV.

**elphel:** Several PHP scripts and a config file that sit aboard the Elphel cameras used in the SCINI ROV.

**iat:** Source for Image Acquisition Tool (IAT), the tool used to capture images from the Elphel Cameras over TCP/IP, and control image exposure/quality.

**pc_pilot:** The LabView source used in the command/control of the SCINI ROV.



All software source materials are licenced under the GNU GPL Licence unless otherwise noted.
