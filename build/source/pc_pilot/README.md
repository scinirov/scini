	This folder contains the LabView souce used in the PC Pilot software used to control the SCINI ROV. The souce can be compiled into a bundled .exe using the LabView Application Builder. The LabView Runtime Engine is needed if one is to build and use the built .exe

Development and running of this code was done using LabView 8.6 on Windows XP 32-bit.

Communication with the SCINI ROV is based on the Video Ray PRO 3 protocol (http://download.videoray.com/developer/docs/Pro3_PC.pdf).
