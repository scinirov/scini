﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="8608001">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="SCINIDustin2_8.6compatable.vi" Type="VI" URL="../SCINIDustin2_8.6compatable.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="VideoRay.rtm" Type="Document" URL="../../Documents and Settings/Dustin Carroll/SCINIOPS Dev/Marcus-jul10/PCPilot446/PCPilot409.llb/VideoRay.rtm"/>
			<Item Name="System Exec.vi" Type="VI" URL="../System Exec.vi"/>
			<Item Name="Empty Picture" Type="VI" URL="../Empty Picture"/>
			<Item Name="Move Pen.vi" Type="VI" URL="../Move Pen.vi"/>
			<Item Name="Draw Line.vi" Type="VI" URL="../Draw Line.vi"/>
			<Item Name="Set Pen State.vi" Type="VI" URL="../Set Pen State.vi"/>
			<Item Name="Draw Circle by Radius.vi" Type="VI" URL="../Draw Circle by Radius.vi"/>
			<Item Name="Draw Arc.vi" Type="VI" URL="../Draw Arc.vi"/>
			<Item Name="Intialize Keyboard.vi" Type="VI" URL="../Intialize Keyboard.vi"/>
			<Item Name="lvinput.dll" Type="Document" URL="../lvinput.dll"/>
			<Item Name="errorList.vi" Type="VI" URL="../errorList.vi"/>
			<Item Name="keyboardAcquire.vi" Type="VI" URL="../keyboardAcquire.vi"/>
			<Item Name="Play Sound File.vi" Type="VI" URL="../Play Sound File.vi"/>
			<Item Name="lvsound2.dll" Type="Document" URL="../lvsound2.dll"/>
			<Item Name="_Get Sound Error From Return Value.vi" Type="VI" URL="../_Get Sound Error From Return Value.vi"/>
			<Item Name="Sound Output Wait.vi" Type="VI" URL="../Sound Output Wait.vi"/>
			<Item Name="Sound Output Task ID.ctl" Type="VI" URL="../Sound Output Task ID.ctl"/>
			<Item Name="JS_setup12.vi" Type="VI" URL="../JS_setup12.vi"/>
			<Item Name="JS_ReadnTranslate05.vi" Type="VI" URL="../JS_ReadnTranslate05.vi"/>
			<Item Name="ListControllers.vi" Type="VI" URL="../ListControllers.vi"/>
			<Item Name="Query Input Devices.vi" Type="VI" URL="../Query Input Devices.vi"/>
			<Item Name="Initialize Joystick.vi" Type="VI" URL="../Initialize Joystick.vi"/>
			<Item Name="joystickAcquire.vi" Type="VI" URL="../joystickAcquire.vi"/>
			<Item Name="Merge Errors.vi" Type="VI" URL="../Merge Errors.vi"/>
			<Item Name="Global1.vi" Type="VI" URL="../Global1.vi"/>
			<Item Name="Acquire Input Data.vi" Type="VI" URL="../Acquire Input Data.vi"/>
			<Item Name="mouseAcquire.vi" Type="VI" URL="../mouseAcquire.vi"/>
			<Item Name="Read_JS_Setup5.vi" Type="VI" URL="../Read_JS_Setup5.vi"/>
			<Item Name="compatOverwrite.vi" Type="VI" URL="../compatOverwrite.vi"/>
			<Item Name="Open_Create_Replace File.vi" Type="VI" URL="../Open_Create_Replace File.vi"/>
			<Item Name="compatFileDialog.vi" Type="VI" URL="../compatFileDialog.vi"/>
			<Item Name="compatOpenFileOperation.vi" Type="VI" URL="../compatOpenFileOperation.vi"/>
			<Item Name="compatCalcOffset.vi" Type="VI" URL="../compatCalcOffset.vi"/>
			<Item Name="Variant Changed__ogtk.vi" Type="VI" URL="../Variant Changed__ogtk.vi"/>
			<Item Name="ManualControls.vi" Type="VI" URL="../ManualControls.vi"/>
			<Item Name="Current VIs Reference__ogtk.vi" Type="VI" URL="../Current VIs Reference__ogtk.vi"/>
			<Item Name="Boolean Trigger__ogtk.vi" Type="VI" URL="../Boolean Trigger__ogtk.vi"/>
			<Item Name="Data Changed__ogtk.vi" Type="VI" URL="../Data Changed__ogtk.vi"/>
			<Item Name="String Changed__ogtk.vi" Type="VI" URL="../String Changed__ogtk.vi"/>
			<Item Name="EXT Changed__ogtk.vi" Type="VI" URL="../EXT Changed__ogtk.vi"/>
			<Item Name="Boolean Changed__ogtk.vi" Type="VI" URL="../Boolean Changed__ogtk.vi"/>
			<Item Name="CDB Changed__ogtk.vi" Type="VI" URL="../CDB Changed__ogtk.vi"/>
			<Item Name="CSG Changed__ogtk.vi" Type="VI" URL="../CSG Changed__ogtk.vi"/>
			<Item Name="CXT Changed__ogtk.vi" Type="VI" URL="../CXT Changed__ogtk.vi"/>
			<Item Name="DBL Changed__ogtk.vi" Type="VI" URL="../DBL Changed__ogtk.vi"/>
			<Item Name="I16 Changed__ogtk.vi" Type="VI" URL="../I16 Changed__ogtk.vi"/>
			<Item Name="I32 Changed__ogtk.vi" Type="VI" URL="../I32 Changed__ogtk.vi"/>
			<Item Name="I8 Changed__ogtk.vi" Type="VI" URL="../I8 Changed__ogtk.vi"/>
			<Item Name="SGL Changed__ogtk.vi" Type="VI" URL="../SGL Changed__ogtk.vi"/>
			<Item Name="U16 Changed__ogtk.vi" Type="VI" URL="../U16 Changed__ogtk.vi"/>
			<Item Name="U32 Changed__ogtk.vi" Type="VI" URL="../U32 Changed__ogtk.vi"/>
			<Item Name="U8 Changed__ogtk.vi" Type="VI" URL="../U8 Changed__ogtk.vi"/>
			<Item Name="1D Boolean Array Changed__ogtk.vi" Type="VI" URL="../1D Boolean Array Changed__ogtk.vi"/>
			<Item Name="1D CDB Array Changed__ogtk.vi" Type="VI" URL="../1D CDB Array Changed__ogtk.vi"/>
			<Item Name="1D CSG Array Changed__ogtk.vi" Type="VI" URL="../1D CSG Array Changed__ogtk.vi"/>
			<Item Name="1D CXT Array Changed__ogtk.vi" Type="VI" URL="../1D CXT Array Changed__ogtk.vi"/>
			<Item Name="1D DBL Array Changed__ogtk.vi" Type="VI" URL="../1D DBL Array Changed__ogtk.vi"/>
			<Item Name="1D EXT Array Changed__ogtk.vi" Type="VI" URL="../1D EXT Array Changed__ogtk.vi"/>
			<Item Name="1D I16 Array Changed__ogtk.vi" Type="VI" URL="../1D I16 Array Changed__ogtk.vi"/>
			<Item Name="1D I32 Array Changed__ogtk.vi" Type="VI" URL="../1D I32 Array Changed__ogtk.vi"/>
			<Item Name="1D I8 Array Changed__ogtk.vi" Type="VI" URL="../1D I8 Array Changed__ogtk.vi"/>
			<Item Name="1D Path Array Changed__ogtk.vi" Type="VI" URL="../1D Path Array Changed__ogtk.vi"/>
			<Item Name="1D SGL Array Changed__ogtk.vi" Type="VI" URL="../1D SGL Array Changed__ogtk.vi"/>
			<Item Name="1D String Array Changed__ogtk.vi" Type="VI" URL="../1D String Array Changed__ogtk.vi"/>
			<Item Name="1D U16 Array Changed__ogtk.vi" Type="VI" URL="../1D U16 Array Changed__ogtk.vi"/>
			<Item Name="1D U32 Array Changed__ogtk.vi" Type="VI" URL="../1D U32 Array Changed__ogtk.vi"/>
			<Item Name="1D U8 Array Changed__ogtk.vi" Type="VI" URL="../1D U8 Array Changed__ogtk.vi"/>
			<Item Name="2D Boolean Array Changed__ogtk.vi" Type="VI" URL="../2D Boolean Array Changed__ogtk.vi"/>
			<Item Name="2D CDB Array Changed__ogtk.vi" Type="VI" URL="../2D CDB Array Changed__ogtk.vi"/>
			<Item Name="2D CSG Array Changed__ogtk.vi" Type="VI" URL="../2D CSG Array Changed__ogtk.vi"/>
			<Item Name="2D CXT Array Changed__ogtk.vi" Type="VI" URL="../2D CXT Array Changed__ogtk.vi"/>
			<Item Name="2D DBL Array Changed__ogtk.vi" Type="VI" URL="../2D DBL Array Changed__ogtk.vi"/>
			<Item Name="2D EXT Array Changed__ogtk.vi" Type="VI" URL="../2D EXT Array Changed__ogtk.vi"/>
			<Item Name="2D I16 Array Changed__ogtk.vi" Type="VI" URL="../2D I16 Array Changed__ogtk.vi"/>
			<Item Name="2D I32 Array Changed__ogtk.vi" Type="VI" URL="../2D I32 Array Changed__ogtk.vi"/>
			<Item Name="2D I8 Array Changed__ogtk.vi" Type="VI" URL="../2D I8 Array Changed__ogtk.vi"/>
			<Item Name="2D Path Array Changed__ogtk.vi" Type="VI" URL="../2D Path Array Changed__ogtk.vi"/>
			<Item Name="2D SGL Array Changed__ogtk.vi" Type="VI" URL="../2D SGL Array Changed__ogtk.vi"/>
			<Item Name="2D String Array Changed__ogtk.vi" Type="VI" URL="../2D String Array Changed__ogtk.vi"/>
			<Item Name="2D U16 Array Changed__ogtk.vi" Type="VI" URL="../2D U16 Array Changed__ogtk.vi"/>
			<Item Name="2D U32 Array Changed__ogtk.vi" Type="VI" URL="../2D U32 Array Changed__ogtk.vi"/>
			<Item Name="2D U8 Array Changed__ogtk.vi" Type="VI" URL="../2D U8 Array Changed__ogtk.vi"/>
			<Item Name="1D Variant Array Changed__ogtk.vi" Type="VI" URL="../1D Variant Array Changed__ogtk.vi"/>
			<Item Name="2D Variant Array Changed__ogtk.vi" Type="VI" URL="../2D Variant Array Changed__ogtk.vi"/>
			<Item Name="SCINIlights.vi" Type="VI" URL="../SCINIlights.vi"/>
			<Item Name="subBuildXYGraph.vi" Type="VI" URL="../subBuildXYGraph.vi"/>
			<Item Name="Dynamic To Waveform Array.vi" Type="VI" URL="../Dynamic To Waveform Array.vi"/>
			<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../ex_CorrectErrorChain.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="TerrorErebus11-2-09" Type="EXE">
				<Property Name="App_applicationGUID" Type="Str">{24EE7527-A8D8-438E-BF08-52030527FA67}</Property>
				<Property Name="App_applicationName" Type="Str">TerrorErebus11-2-09.exe</Property>
				<Property Name="App_fileDescription" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="App_fileVersion.major" Type="Int">1</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{456EA61D-EEAB-4F83-A9BE-1BC5C01D26B1}</Property>
				<Property Name="App_INI_GUID" Type="Str">{EF9AD60D-C87A-45C3-8F78-61A2D7D4AF4C}</Property>
				<Property Name="App_internalName" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="App_legalCopyright" Type="Str">Copyright © 2009 </Property>
				<Property Name="App_productName" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="Bld_buildSpecName" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">TerrorErebus11-2-09.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/NI_AB_PROJECTNAME/internal.llb</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/C/NI_AB_PROJECTNAME/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{D70C2624-72B5-4456-B198-0B4A8AB68C4B}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/SCINIDustin2_8.6compatable.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
			</Item>
			<Item Name="TerrorErebus11-2-09Installer" Type="Installer">
				<Property Name="AutoIncrement" Type="Bool">true</Property>
				<Property Name="BuildLabel" Type="Str">TerrorErebus11-2-09Installer</Property>
				<Property Name="BuildLocation" Type="Path">../../../../../TerrorErebus11-2-09Installer</Property>
				<Property Name="DirInfo.Count" Type="Int">2</Property>
				<Property Name="DirInfo.DefaultDir" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="DirInfo[0].DirName" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="DirInfo[0].DirTag" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="DirInfo[0].ParentTag" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="DirInfo[1].DirName" Type="Str">data</Property>
				<Property Name="DirInfo[1].DirTag" Type="Str">{E059CF13-8259-4BC8-8D42-ADD9080D22AA}</Property>
				<Property Name="DirInfo[1].ParentTag" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="DistID" Type="Str">{BCB2DDCB-C2C9-48C3-AE1E-DFDD0B7EA45B}</Property>
				<Property Name="DistParts.Count" Type="Int">1</Property>
				<Property Name="DistPartsInfo[0].FlavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPartsInfo[0].ProductID" Type="Str">{2CA542BC-E002-4064-84DB-49B3E558A26D}</Property>
				<Property Name="DistPartsInfo[0].ProductName" Type="Str">NI LabVIEW Run-Time Engine 8.6</Property>
				<Property Name="DistPartsInfo[0].UpgradeCode" Type="Str">{7975A1CC-5DCA-4997-EE8C-C1903BA18512}</Property>
				<Property Name="FileInfo.Count" Type="Int">3</Property>
				<Property Name="FileInfo[0].DirTag" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="FileInfo[0].FileName" Type="Str">TerrorErebus11-2-09.exe</Property>
				<Property Name="FileInfo[0].FileTag" Type="Str">{24EE7527-A8D8-438E-BF08-52030527FA67}</Property>
				<Property Name="FileInfo[0].Type" Type="Int">3</Property>
				<Property Name="FileInfo[0].TypeID" Type="Ref">/My Computer/Build Specifications/TerrorErebus11-2-09</Property>
				<Property Name="FileInfo[1].DirTag" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="FileInfo[1].FileName" Type="Str">TerrorErebus11-2-09.aliases</Property>
				<Property Name="FileInfo[1].FileTag" Type="Str">{456EA61D-EEAB-4F83-A9BE-1BC5C01D26B1}</Property>
				<Property Name="FileInfo[1].Type" Type="Int">3</Property>
				<Property Name="FileInfo[1].TypeID" Type="Ref">/My Computer/Build Specifications/TerrorErebus11-2-09</Property>
				<Property Name="FileInfo[2].DirTag" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="FileInfo[2].FileName" Type="Str">TerrorErebus11-2-09.ini</Property>
				<Property Name="FileInfo[2].FileTag" Type="Str">{EF9AD60D-C87A-45C3-8F78-61A2D7D4AF4C}</Property>
				<Property Name="FileInfo[2].Type" Type="Int">3</Property>
				<Property Name="FileInfo[2].TypeID" Type="Ref">/My Computer/Build Specifications/TerrorErebus11-2-09</Property>
				<Property Name="InstSpecVersion" Type="Str">8608001</Property>
				<Property Name="LicenseFile" Type="Ref"></Property>
				<Property Name="OSCheck" Type="Int">0</Property>
				<Property Name="OSCheck_Vista" Type="Bool">false</Property>
				<Property Name="ProductName" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="ProductVersion" Type="Str">1.0.1</Property>
				<Property Name="ReadmeFile" Type="Ref"></Property>
				<Property Name="ShortcutInfo.Count" Type="Int">1</Property>
				<Property Name="ShortcutInfo[0].DirTag" Type="Str">{B9E310F1-839C-48B7-8CAE-33000780C26E}</Property>
				<Property Name="ShortcutInfo[0].FileTag" Type="Str">{24EE7527-A8D8-438E-BF08-52030527FA67}</Property>
				<Property Name="ShortcutInfo[0].FileTagDir" Type="Str">{76324985-3702-4936-BC7F-4B492FBDB05D}</Property>
				<Property Name="ShortcutInfo[0].Name" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="ShortcutInfo[0].SubDir" Type="Str">TerrorErebus11-2-09</Property>
				<Property Name="UpgradeCode" Type="Str">{5E887C75-DDCD-4E23-8E85-3CF4556DB2F7}</Property>
			</Item>
		</Item>
	</Item>
</Project>
