<?php
/*! Copyright (C) 2009 Apertus, All Rights Reserved
 *! Author : Apertus Team
 *! Description: php script used to set camera parameters
 *! called by the java application via http request
-----------------------------------------------------------------------------**
 *!
 *!  This program is free software: you can redistribute it and/or modify
 *!  it under the terms of the GNU General Public License as published by
 *!  the Free Software Foundation, either version 3 of the License, or
 *!  (at your option) any later version.
 *!
 *!  This program is distributed in the hope that it will be useful,
 *!  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *!  GNU General Public License for more details.
 *!
 *!  You should have received a copy of the GNU General Public License
 *!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *!
-----------------------------------------------------------------------------**/

$val = $_GET["description"];

// debugging
// echo "val.chr0: ".$val.chr(0)."<br />\n";

// ImageDescription
if ( $val!==null)  elphel_set_exif_field(0x10e, $val.chr(0));
// UserComment
// if ( $val!==null ) elphel_set_exif_field(0x010e, $val.chr(0));

?>
