This folder contains the source that is compiled and loaded on to SCINI's three Atmel 328 micro controllers. Atmel Studio was used in development, and you will find an Solution and Project file.

Fuses can be left at their default values. An AVRISP mkII or other programmer can be used to flash the microcontrollers.

For licencing information, please refer to LICENCE.txt
