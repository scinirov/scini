/*
SCINI MCU Code

Copyright (C) <2010>  <Dustin Carrol - Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include "scini.h"

//------------------------------DEFINE BOARD HERE (MAIN, CAMERA, IMU)
#define THIS_BOARD_IS MAIN
//------------------------------DEFINE BOARD HERE (MAIN, CAMERA, IMU)

#define FAIL_SAFE_TIME 1000000 //hz

int initializeIO(void);				//initialize IO
int initializeADC(void);			//initialize ADC
int initializeWatchdog(void);		//initialize watchdog timer
int initializePWM(void);			//initialize pulse width modulation
int FailSafeMode(void);				//enter fail safe mode, kill thrusters
int BootloadReset(void);

int integrateIMU(void);				//integrate heading, roll, and tilt to reduce noise
int integrateXYZ(void);				//integrate IMU x,y,z accel
int integrateSensor(void); 			//integrate temperature, humidity, and pressure

int KillThrusters (void);			//stop thrusters
int DelayMS(uint16_t x);			//general purpose delay

static int UARTPutChar(char c, FILE *stream);	//UART put char
uint8_t UARTGetChar();							//UART get char

static FILE mystdout = FDEV_SETUP_STREAM(UARTPutChar, NULL, _FDEV_SETUP_WRITE);

int SetLights(int setting);	//adjust lights
int SetLightOCR(void);		//set output compare register PWM for buckpucks
int FlashLights(uint32_t board);	//do "happy dance"

uint16_t rawAC, rawTilt, rawRoll, rawXAccel, rawYAccel, rawZAccel = 0, rawSC;
uint16_t tilt, roll, xAccel, yAccel, zAccel = 0;

int tiltCounter, rollCounter, xAccelCounter, yAccelCounter, zAccelCounter = 0;
int imuReadCounter = 0;
int xyzReadCounter = 0;
int channelADC = 0;

int imuIntegrationTime = 1;	//default integration times (adc cycles)
int xyzIntegrationTime = 1;
int sensorIntegrationTime = 1;

int reset = 0; //bootload reset flag

uint16_t rawTemp, rawHumidity, rawPressure, rawAC24v, rawAC12v = 0;
uint16_t temp, humidity, pressure = 0; // pressurePulseLength = 0;
int tempCounter, humidityCounter, pressureCounter = 0;
int sensorReadCounter = 0;

uint16_t startTime = 0;	//start time for depth sensor PWM input capture

uint8_t lightSetting[4];

uint32_t thisBoard;	//board identifier
uint32_t failSafeCounter = 0;

uint8_t dataIn[DIN_NUM_BYTES];	//data recieved from labview

uint32_t mainAft, forwVert, aftVert, forwHorz, aftHorz, cameraTilt, cameraFocus, gripper = 1500; //PWM's default to 1.5ms

uint16_t PRO3_totalChecksum = 0;

/*
Data recieved:
#define DIN_BOARD_TYPE 0
#define DIN_BOARD_TYPE_LEN 4
#define DIN_MAIN 4
#define DIN_FORW_VERT 5
#define DIN_AFT_VERT 6
#define DIN_FORW_HORZ 7
#define DIN_AFT_HORZ 8
#define DIN_LED1 9
#define DIN_LED2 10
#define DIN_CAM_TILT 11
#define DIN_CAM_FOCUS 12
#define DIN_LASER 13
#define DIN_LED3 14
#define DIN_LED4 15
#define DIN_CAMERA 16
#define DIN_IMU_INT 17
#define DIN_XYZ_INT 18
#define DIN_SENSOR_INT 19
#define DIN_RESET 20
#define DIN_GRIPPER 21
*/

int main (void)
{	
	MCUSR = 0; //reset watchdog timer to avoid accidental reset
	WDTCSR = (1<<WDCE) | (0<<WDE);

	//sbi(DDRB, 3); //OCR2A, pin 17, Buckpuck1 
	sbi(DDRD, 3); 	//OCR2B, pin 5   Buckpuck2
	sbi(DDRD, 6); 	//OCR0A, pin 12, Buckpuck3
	sbi(DDRD, 5); 	//OCR0B, pin 11, Buckpuck4
		
	//sbi(PORTB,3);	//set Buckpuck1 high
	sbi(PORTD,3); 	//set Buckpuck2 high
	sbi(PORTD,6); 	//set Buckpuck3 high	
	sbi(PORTD,5); 	//set Buckpuck4 high

	DelayMS(2000);	//2 second delay to wait for 300v 

	char ch;
	uint8_t bytesRead = 0;
	uint32_t boardType = 0;
	uint8_t thisBoardActive;

	uint16_t totalChecksum;
	uint8_t totalChecksumFlag = 0;

	thisBoard = THIS_BOARD_IS;	//set board define

	reset = 0;				//reset off

	cli();					//clear global interrupts
	initializeIO();			//initialize IO pin directions
	initializeADC();		//initialize ADC conversion
	initializePWM();		//initialize pulse width modulations
	sei();

	cbi(PORTD, CAM_ENABLE);	//turn off 12V power supply to disable servo
	
	FlashLights(thisBoard);	//start "happy dance"

	sbi(PORTD, LED_ENABLE);	//turn on 3.3V power supply to enable lasers. laser on by default

	initializeWatchdog();

	while(1)
	{
		asm volatile ("wdr");	//reset the watchdog timer
		cbi (PORTD, TRANSMIT_EN);  //listen on RS485
		
    	ch = UARTGetChar();

		// keep reading characters until we get a start code (the # character)      
	  	while (ch != 0x23) 
	  	{
			ch = UARTGetChar();
	  	}

      	bytesRead = 0;  
	  	while (bytesRead < DIN_NUM_BYTES) //read in full PRO3 data stream 
	  	{
			dataIn[bytesRead] = UARTGetChar(reset);
			bytesRead++;
      	}

		boardType = 0;
		for (int i = 0; i < DIN_BOARD_TYPE_LEN; i++) //determine which board is requested to respond 
		{
			boardType = boardType | ((uint32_t)dataIn[DIN_BOARD_TYPE+i] << (8*(3-i)));
    		thisBoardActive = (boardType == thisBoard);
		}

		PRO3_totalChecksum = dataIn[DIN_PRO3_TOTAL_CHECKSUM];
				
		totalChecksum = 0;
		for (int i=4; i < DIN_NUM_BYTES-1  ; i++)
		{
			totalChecksum = totalChecksum ^ dataIn[i]; //XOR to checksum
	  	}
		totalChecksumFlag = (totalChecksum != PRO3_totalChecksum) ? 1 : 0;

		if(!totalChecksumFlag) //if passed checksum
		{
			imuIntegrationTime = dataIn[DIN_IMU_INT];
			xyzIntegrationTime = dataIn[DIN_XYZ_INT];
			sensorIntegrationTime = dataIn[DIN_SENSOR_INT];

			reset = dataIn[DIN_RESET];
 
			if (thisBoard == MAIN) //set motor control PWM
			{
				mainAft = dataIn[DIN_MAIN]; 
				forwVert = dataIn[DIN_FORW_VERT];
				forwHorz = dataIn[DIN_FORW_HORZ];
				aftVert = dataIn[DIN_AFT_VERT];
				aftHorz = dataIn[DIN_AFT_HORZ];
		
				OCR0A = 255 - ((((aftHorz * 1000) / 255) + 1000) / 35);  //Main Aft, pin 11
				OCR0B = 255 - ((((mainAft * 1000) / 255) + 1000) / 35);  //Aft Horz, pin 12
			
				OCR2A = 255 - ((((forwHorz * 1000) / 255) + 1000) / 35); //Forw Horz, pin 17

				OCR1A = 255 - ((((aftVert * 1000) / 255) + 1000) / 35);  //Aft Vert, pin 15
				OCR1B = 255 - ((((forwVert * 1000) / 255) + 1000) / 35); //Forw Vert, pin 16
	    	}

			else if (thisBoard == CAMERA)
			{		
				cameraTilt = dataIn[DIN_CAM_TILT];
				cameraFocus = dataIn[DIN_CAM_FOCUS];
				gripper = dataIn[DIN_GRIPPER];

				OCR1A = 255 - ((((cameraTilt * 1000) / 255) + 1000) / 35); 	//camera tilt, pin 15
				OCR1B = 255 - ((((cameraFocus * 1000) / 255) + 1000) / 35); //camera focus, pin 16
				OCR2A = 255 - ((((gripper * 1000) / 255) + 1000) / 35); 	//gripper, pin 17
			
	        	lightSetting[0] = dataIn[DIN_LED1]; 	//forward left, OCR0A
	        	lightSetting[1] = dataIn[DIN_LED2]; 	//forward right, OCR0B
	        	//lightSetting[2] = dataIn[DIN_LED3]; 	//down left, OCR2A
	        	lightSetting[3] = dataIn[DIN_LED4]; 	//down right, OCR2B

	        	SetLightOCR();

				if (dataIn[DIN_LASER])
				{
					sbi(PORTD, LED_ENABLE);	//turn on 3.3V power supply.
				}
				else
				{
					cbi(PORTD, LED_ENABLE);	//turn off 3.3V power supply.
				}

				if (dataIn[DIN_CAMERA])
				{
					sbi(PORTD, CAM_ENABLE);	//turn on 12V power supply.
				}
				else
				{
					cbi(PORTD, CAM_ENABLE);	//turn off 12V power supply.
				}
			}

			if (thisBoardActive)	//if request for board to respond, response is 32 bytes
			{
				sbi (PORTD, TRANSMIT_EN);	//transmit on RS485
				if (thisBoard == MAIN)
				{
					printf ("%8lx %4x %4x %4x %4x %4x %4x %c %c", MAIN_RESPONSE, humidity, temp, pressure, tilt, roll, totalChecksumFlag, 0xd, 0xa);
				}
				else if (thisBoard == IMU)
				{
					printf ("%8lx %4x %4x %4x %4x %4x %4x %c %c", IMU_RESPONSE, tilt, roll, xAccel, yAccel, rawAC, totalChecksumFlag, 0xd, 0xa);
				}
				else if (thisBoard == CAMERA)
				{
					printf ("%8lx %4x %4x %4x %4x %4x %4x %c %c", CAMERA_RESPONSE, humidity, temp, rawAC24v, rawAC12v, totalChecksumFlag, 0x0, 0xd, 0xa);
				}
			}

			//bootload reset addresses individual boards
		    //if bootloader reset, don't kick the watchdog so micro resets into bootloader
			if ((reset == 1) && (thisBoard == MAIN)) 
			{ 
				BootloadReset(); 
			}
			else if ((reset == 2) && (thisBoard == IMU)) 
			{ 
				BootloadReset(); 
			}
			else if((reset == 3) && (thisBoard == CAMERA)) 
			{ 
				BootloadReset(); 
			} 
			else //no reset flag, continue as normal
			{
				asm volatile ("wdr");	//reset the watchdog timer
			}
		}
	}
	return(0);
}


int initializeIO(void)
{
	DDRB = 0b11101111; 		//PB4 is MISO
	DDRD = 0b11111110; 		//RX is input on PD0

	UBRR0H = MYUBRR >> 8;	//set UART Baud rate

    UBRR0L = MYUBRR;
    UCSR0B = (1<<RXEN0)|(1<<TXEN0);

    stdout = &mystdout; 	//Required for printf initialization
	return 0;
}


int initializeADC(void)
{
	roll = 0; rawRoll = 0; rollCounter = 0;
	tilt = 0; rawTilt = 0; tiltCounter = 0;
	xAccel = 0; rawXAccel = 0; xAccelCounter = 0;
	yAccel = 0; rawYAccel = 0; yAccelCounter = 0;
	zAccel = 0; rawZAccel = 0; zAccelCounter =	0;
	
	imuReadCounter = 0;

	temp = 0; rawTemp = 0; tempCounter = 0;
	humidity = 0; rawHumidity = 0; humidityCounter = 0;
	pressure = 0; rawPressure = 0; pressureCounter = 0;
	//pressurePulseLength = 0;
	
	rawAC24v = 0; rawAC12v = 0; rawSC = 0;

	channelADC = 0;

	ADCSRA |= (1 << ADEN); //enable ADC
	ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); 	 //128 Prescaler

    ADMUX = 0x40;	//start at ADC0 channel, use AVCC with cap at VREF

	DDRC = 0xc0;	// enable all of portc as input
    DIDR0 = 0x3f;	// disable digital input on all portC pins (power saving)

    MCUCR |= (1<<SE);		//set enable sleep
	MCUCR |= (1 << SM0);	//setting sleep mode to "ADC Noise Reduction"

	ADCSRA &= ~(1 << ADIF); //clear ADC interrupt flag
    ADCSRA |= (1 << ADIE);	//enable ADC interrupt
	ADCSRA |= (1 << ADSC);  //start ADC

	__asm volatile ("sleep"); 	//go to sleep
	MCUCR &= ~(1<<SE); 			//clear enable sleep

	return 0;
}


ISR(ADC_vect)
{	
	//IMU board ADC
	if(thisBoard == IMU)
	{
		cli(); //clear global interrupts

	  	uint8_t admuxHi = (ADMUX & 0b11000000);	//single conversion
		uint16_t rawADC = 0;
		uint16_t aL = 0;
		uint16_t hL = 0;

	    aL = ADCL; //low byte of 10 bit adc conversion
		hL = ADCH; //high byte 

		rawADC = (hL << 8) | aL;

		switch(channelADC)
		{
		case 0:	//ADC0, pin 23, AC sensor board
			rawAC = rawADC;
			break;

		case 1: //ADC1, pin 24, IMU y rate, (roll rate)
			rawYAccel += rawADC;
			break;

		case 2: //ADC2, pin 25, IMU x rate (turn rate) 
			rawXAccel += rawADC;
			break;

		case 3: //ADC3, pin 26, IMU roll
			rawRoll += rawADC;
			rollCounter++;
			break;

		case 5: //ADC5, pin 28, IMU tilt
			rawTilt += rawADC;
			tiltCounter++;
			break;
		}
		
		channelADC++;			//increment adc channel
		if (channelADC > 5)		//channels 1-5 have been converted and read
		{
			channelADC = 0;		//restart on channel 0
			imuReadCounter++;
			xyzReadCounter++;
		}
		ADMUX = admuxHi | channelADC;	//increment ADMUX channel

		
		if(xyzReadCounter > xyzIntegrationTime)
		{
			integrateXYZ();
			xyzReadCounter = 0;
		}
		else if(imuReadCounter > imuIntegrationTime)
		{
			integrateIMU();
			imuReadCounter = 0;
		}
		sei(); //enable global interrupts
	}
	//MAIN board ADC
	if(thisBoard == MAIN)
	{
		cli();
	  	uint8_t admuxHi = (ADMUX & 0b11000000);	//single conversion
		uint16_t rawADC = 0;
		uint16_t aL = 0;
		uint16_t hL = 0;

	    aL = ADCL;
		hL = ADCH;

		rawADC = (hL << 8) | aL;

		switch(channelADC)
		{
		case 0: //ADC0, pin 23, analog depth sensor
			rawPressure += rawADC;
			pressureCounter++;
			break;

		case 1: //ADC1, pin 24, humidity sensor
			rawHumidity += rawADC;
			humidityCounter++;
			break;

		case 2: //ADC2, pin 25, temperature sensor
			rawTemp += rawADC;
			tempCounter++;
			break;

		case 3: //ADC3, pin 26, IMU roll
			rawRoll += rawADC;
			rollCounter++;
			break;

		case 5: //ADC5, pin 28, IMU tilt
			rawTilt += rawADC;
			tiltCounter++;
			break;
		}
		
		channelADC++;			//increment adc channel
		if (channelADC > 5)		//channels 1-5 have been converted and read
		{
			channelADC = 0;		//restart on channel 0
			sensorReadCounter++;
			imuReadCounter++;
			xyzReadCounter++;
		}

		ADMUX = admuxHi | channelADC;	//increment ADMUX channel
		
		if(sensorReadCounter > sensorIntegrationTime)
		{
			integrateSensor();
			sensorReadCounter = 0;
		}
		
		else if(xyzReadCounter > xyzIntegrationTime)
		{
			integrateXYZ();
			xyzReadCounter = 0;
		}
		else if(imuReadCounter > imuIntegrationTime)
		{
			integrateIMU();
			imuReadCounter = 0;
		}
	}
	//CAMERA board ADC
	else if((thisBoard == CAMERA))
	{
		cli();
	  	uint8_t admuxHi = (ADMUX & 0b11000000);	//single conversion
		uint16_t rawADC = 0;
		uint16_t aL = 0;
		uint16_t hL = 0;

	    aL = ADCL;
		hL = ADCH;

		rawADC = (hL << 8) | aL;

		switch(channelADC)
		{
		case 0: //ADC0, pin 23, SC vicor 24v
			rawAC24v = rawADC;
			break;

		case 1: //ADC1, pin 24, humidity sensor
			rawHumidity += rawADC;
			humidityCounter++;
			break;

		case 2: //ADC2, pin 25, temperature sensor
			rawTemp += rawADC;
			tempCounter++;
			break;
		
		case 3: //ADC3, pin 26, SC vicor 12v
			rawAC12v = rawADC;		
		}

		channelADC++;			//increment adc channel
		if (channelADC > 5)		//channels 1-5 have been converted and read
		{
			channelADC = 0;		//restart on channel 0
			sensorReadCounter++;
		}
		ADMUX = admuxHi | channelADC;	//increment ADMUX channel

		if(sensorReadCounter > sensorIntegrationTime)
		{
			integrateSensor();
			sensorReadCounter = 0;
		}		
	}
	ADCSRA |= (1 << ADSC); //start adc conversion
}


int initializePWM(void)
{
	sbi(DDRD, 6); //OC0A,  pin 12, Aft Horz,  Buckpuck3
	sbi(DDRD, 5); //OC0B,  pin 11, Main Aft,  Buckpuck4 	
	sbi(DDRB, 1); //OC1A,  pin 15, Aft Vert,  Tilt, 
	sbi(DDRB, 2); //OC1B,  pin 16, Forw Vert, Focus
	sbi(DDRB, 3); //OC2A,  pin 17, Forw Horz, Buckpuck1 
	sbi(DDRD,3);  //OCR2B, pin 5     	      Buckpuck2

	if (thisBoard == MAIN)
  	{
		TCCR0A = 1 << COM0A1 | 1 << COM0A0 | 1 << COM0B1 | 1 << COM0B0 | 1 << WGM01 | 1 << WGM00; //aft horizontal pin 12
		TCCR0B = 0 << FOC0A | 0 << FOC0B | 0 << WGM02 | 1 << CS02 | 0 << CS01 | 0 << CS00; //main thruster pin 11

		TCCR2A = 1 << COM2A1 | 1 << COM2A0 | 1 << COM2B1 | 1 << COM2B0 | 1 << WGM21 | 1 << WGM20; //forw horizontal pin 17 	 
		TCCR2B = 0 << FOC2A | 0 << FOC2B |  0 << WGM22 | 1 << CS22 | 1 << CS21 | 0 << CS20; 

		TCCR1A = 1 << COM1A1 | 1 << COM1A0 | 1 << COM1B1 | 1 << COM1B0 | 0 << WGM13| 1 << WGM12| 0 << WGM11 | 1 << WGM10; 
		TCCR1B = 0x4C; //setting bits by defined name will not set CS10 low
	}
	else if (thisBoard == IMU)
	{
		//cbi (DDRB, 0); //pressure sensor input, for SSP-1
	
		TCCR1A = (0 << COM1A0) | (0 << COM1B1) | (0 << WGM11) | (0 << WGM10); //normal mode
		TCCR1B = (0 << WGM13) | (0 << WGM12) | (0 << CS12) | (1 << CS11) | (0 << CS10); //normal mode, prescaler / 8

		TCCR1B |= (1 << ICNC1) | (1 << ICES1); //enable noise cancellation, capture the next rising edge
		//TIMSK1 |= (1 << ICIE1); //enable input capture interrupt, for SSP-1
	}
	else if (thisBoard == CAMERA)
	{
		TCCR0A = 1 << COM0A1 | 1 << COM0A0 | 1 << COM0B1 | 1 << COM0B0 | 1 << WGM01 | 1 << WGM00; //buckpuck3, pin 12 
		TCCR0B = 0 << FOC0A | 0 << FOC0B | 0 << WGM02 | 0 << CS02 | 0 << CS01 | 1 << CS00; //buckpuck4, pin 11

		//TCCR2A = 1 << COM2A1 | 1 << COM2A0 | 1 << COM2B1 | 1 << COM2B0 | 1 << WGM21 | 1 << WGM20; //buckpuck 1, pin 17
		//TCCR2B = 0 << FOC2A | 0 << FOC2B |  0 << WGM22 | 0 << CS22 | 0 << CS21 | 1 << CS20; //buckpuck2, pin 5
		
		//To enable gripper 
		TCCR2A = 1 << COM2A1 | 1 << COM2A0 | 1 << COM2B1 | 1 << COM2B0 | 1 << WGM21 | 1 << WGM20; //forw horizontal pin 17 	 
		TCCR2B = 0 << FOC2A | 0 << FOC2B |  0 << WGM22 | 1 << CS22 | 1 << CS21 | 0 << CS20; 

		TCCR1A = 1 << COM1A1 | 1 << COM1A0 | 1 << COM1B1 | 1 << COM1B0 | 0 << WGM13| 1 << WGM12| 0 << WGM11 | 1 << WGM10; //camera tilt, pin 15 
		TCCR1B = 0x4C; //setting bits by defined name will not set CS10 low, camera focus, pin 16
	}
  	return 0;
}


//watchdog initialization per example in AVR datasheet (8161C-AVR-05/09), page 53
int initializeWatchdog(void)
{
	cli();  //disable any innterrupts to prevent corruption of the timed initialization sequence
  	asm volatile ("wdr");  //reset the watchdog
  	WDTCSR |= (1<<WDCE) | (1<<WDE);  //enable the WDT
  	WDTCSR = (1<<WDE) | (1<<WDP2) | (1<<WDP1) | (1 <<WDP0);  //set the timeout interval to ~ 2 seconds 
  	sei();
  	return 0;
}

/* SSP1 pressure sensor 
ISR(TIMER1_CAPT_vect)	//capture depth sensor PWM 
{
	cli();

	if(TCCR1B & (1 << ICES1)) //rising edge of a pulse 
	{
		startTime = ICR1;
		TCCR1B &= ~(1 << ICES1); //set for falling edge capture
	}
	else //end of a pulse, compute pulse length
	{
		pressurePulseLength = ICR1 - startTime;	

		TCCR1B |= 1 << ICES1;	//set for rising edge capture
	}
	sei();
}
*/

int FailSafeMode(void)
{
	if (thisBoard == MAIN)
	{
		KillThrusters();
	}
	else if (thisBoard == CAMERA)
	{
		lightSetting[0] = 30;
		lightSetting[1] = 30;
		lightSetting[2] = 30;
		lightSetting[3] = 30;

	    SetLightOCR();
		cbi(PORTD, CAM_ENABLE);	//turn off 3.3V power supply to disable servo
	}
	sbi(PORTD, LED_ENABLE);	//turn on 3.3V power supply to enable lasers
	return 0;
}


int BootloadReset(void)
{
	for (int x=0; x<100; x++)
	{
		DelayMS(100); //pause for .1 second, then kick watchdog
		asm volatile ("wdr");	//reset the watchdog timer
	}
	
	while(1); //go into infinite loop so watchdog timer resets the micro

	return 0;
}


int integrateIMU(void)
{
	tilt = rawTilt / tiltCounter;
	roll = rawRoll / rollCounter;

	rawTilt = 0; tiltCounter = 0;
	rawRoll = 0; rollCounter = 0;

	return 0;
}


int integrateXYZ(void)
{
	xAccel = rawXAccel;
	yAccel = rawYAccel;

	rawXAccel = 0; xAccelCounter = 0;
	rawYAccel = 0; yAccelCounter = 0;

	return 0;
}


int integrateSensor(void)
{
	temp = rawTemp / tempCounter;
	humidity = rawHumidity / humidityCounter;
	pressure = rawPressure / pressureCounter;

	rawTemp = 0; tempCounter = 0;
	rawHumidity = 0; humidityCounter = 0;
	rawPressure = 0; pressureCounter = 0;

	return 0;
}


static int UARTPutChar(char c, FILE *stream)
{
    if (c == '\n') UARTPutChar('\r', stream);

    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = c;

    return 0;
}


uint8_t UARTGetChar(int reset)
{
	//waits for data to be present in the receive buffer by checking the RXC Flag
    while(!(UCSR0A & (1<<RXC0))) 
	{
		asm volatile ("wdr");	//reset watchdog timer

		failSafeCounter++;		
		
		if(failSafeCounter > FAIL_SAFE_TIME)	//after ~ 1 second, enter fail safe
		{
			FailSafeMode();	//kill thrusters, set lights to fail safe mode
		}
	} 
	failSafeCounter = 0;	//reset failsafe

    return(UDR0);	//return data byte
}


int DelayMS(uint16_t x)
{
	int y, z = 0;

  	for (; x>0; x--)
  	{
  		for (y=0; y<80; y++)
		{
      		for (z=0; z<20; z++)
			{
        		asm volatile ("nop");
      		}		
    	}
  	}
	return 0;
}


int KillThrusters(void)
{
	int neutral = 1500; //1.5ms PWM

    OCR0A = 255 - (neutral / 35);  //Main Aft, pin 11
	OCR0B = 255 - (neutral / 35);  //Aft Horz, pin 12
	OCR2A = 255 - (neutral / 35);  //Forw Horz, pin 17
	OCR1A = 255 - (neutral / 35);  //Aft Vert, pin 15
	OCR1B = 255 - (neutral / 35);  //Forw Vert, pin 16

	return 0;
}


int SetLightOCR(void)
{
	int pwmSetting = 0;

  	for (int i=0;i<4;i++)
	{
		pwmSetting = lightSetting[i];

    	switch(i)
		{
      		case 0: OCR0A = pwmSetting; //Buckpuck3
			break;

			case 1: OCR0B = pwmSetting; //Buckpuck4
			break;

			//case 2: OCR2A = pwmSetting; //Buckpuck1, disabled due to gripper
			//break;

			case 3: OCR2B = pwmSetting; //Buckpuck2
			break;
    	}
  	}
	return 0;
}


int FlashLights(uint32_t board)
{
	
	for (int k = 0; k < 3; k++)
	{
	    for (int i=0; i<255; i++)	//sweep lights
		{
	        lightSetting[0] = i;
			lightSetting[1] = i;
			//lightSetting[2] = i;
			lightSetting[3] = i;
			
			if(board == CAMERA)
			{
	        	SetLightOCR();
			}

			if (i < 128)	//turn lasers on for half of a light sweep 
			{ 
				sbi(PORTD, LED_ENABLE);	//turn on 3.3V power supply to enable lasers.
			}
			else
			{
				cbi(PORTD, LED_ENABLE);	//turn on 3.3V power supply to enable lasers.
			}
			DelayMS(3);
			asm volatile ("wdr");	//reset the watchdog timer
	    }
	}

	lightSetting[0] = 0;	//turn lights off when happy dance is complete
	lightSetting[1] = 0;
	//lightSetting[2] = 0;
	lightSetting[3] = 0;
	SetLightOCR();

	return 0;
}


int SetLights(int setting)
{
	for(int i=0; i<4; i++) 
	{
		lightSetting[i] = setting;
    	SetLightOCR();
	}
	return 0;
}
