/*
SCINI MCU Code

Copyright (C) <2010>  <Dustin Carrol - Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#define sbi(var, mask)   ((var) |= (uint8_t)(1 << mask))
#define cbi(var, mask)   ((var) &= (uint8_t)~(1 << mask))

#define FOSC 7372800	//board crystal frequency in hz, ~7.3 mhz
		
#define MAIN 0xa83f9c3c
#define MAIN_RESPONSE 0xa83f9c3d

#define IMU 0xfa429c24
#define IMU_RESPONSE 0xfa429c25

#define CAMERA 0x809c084e
#define CAMERA_RESPONSE 0x809c084f

#define BAUD 115200 //baud rate of UART that XPORT->ENOP->PCPILOT will communicate with
#define SEND_BACK 1
#define MYUBRR (((((FOSC*10)/(16L * BAUD)) +5) /10)-1)

#define TRANSMIT_EN 2    //PD2, RS485 direction control
#define LED_ENABLE 7     //PD7, enable laser
#define CAM_ENABLE 4	 //PD4, enable camera power

//PRO3 header
#define DIN_BOARD_TYPE 0
#define DIN_BOARD_TYPE_LEN 4

//PRO3 data
#define DIN_MAIN 4
#define DIN_FORW_VERT 5
#define DIN_AFT_VERT 6
#define DIN_FORW_HORZ 7
#define DIN_AFT_HORZ 8
#define DIN_LED1 9
#define DIN_LED2 10
#define DIN_CAM_TILT 11
#define DIN_CAM_FOCUS 12
#define DIN_LASER 13
#define DIN_LED3 14
#define DIN_LED4 15
#define DIN_CAMERA 16
#define DIN_IMU_INT 17
#define DIN_XYZ_INT 18
#define DIN_SENSOR_INT 19
#define DIN_RESET 20
#define DIN_GRIPPER 21
#define DIN_PRO3_TOTAL_CHECKSUM 22

#define DIN_NUM_BYTES 23 //always 1 greater than the number of bytes
