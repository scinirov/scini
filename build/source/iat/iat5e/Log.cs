﻿/*
Log.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ImageAnnotationTool
{

    static class DateTimeFormat
    {
        const string dateTimeFormat = "yyyy-MM-dd H:mm:ss.fff ";
        const string dateTimeFormat_file = "yyyy_MM_dd_H_mm_ss_fff"; //loose non filesystem safe chars
        public static string file(DateTime time)
        {
            return time.ToUniversalTime().ToString(dateTimeFormat_file);
        }
        public static string txt(DateTime time)
        {
            return time.ToUniversalTime().ToString(dateTimeFormat);
        }
    }
}
