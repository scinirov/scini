﻿/*
AnnotationControl.xaml.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Threading;

namespace ImageAnnotationTool
{
    public partial class AnnotationControl : UserControl
    {
        DateTime GrabTime;

        System.Windows.Threading.DispatcherTimer PositionCommCheck;

/*      private int heading = 0;
        private double depth = 0;
        private int cameraTilt = 0;
        private int streamExposure = 0;
        private int decimation = 0;  */

        private double depth = 0;
        private int tilt = 0;
        private int heading = 0;
        private int light = 0;
        private int cameraTilt = 0;

        private int mainTemp = 0; // main bottle temperature
        private int camTemp = 0;  // camera bottle temperature
        private int mainHum = 0;  // main bottle humidity
        private int camHum = 0;   // camera bottle humidity

        private int roll = 0;

        private int forwardBackCruise = 0;
        private int leftRightCruise = 0;
        private int upDownCruise = 0;

        private int mainAft = 0;
        private int altForward = 0;
        private int altRear = 0;
        private int horizForward = 0;
        private int horizRear = 0;

        private int streamExposure = 0;
        private int decimation = 0;

        private MainControlWindow mainWindow;

        public AnnotationControl()
        {
            InitializeComponent();
            
            PositionCommCheck = new DispatcherTimer();
            PositionCommCheck.Interval = new TimeSpan(0, 0, 0, 0, 1000); ;
            PositionCommCheck.Tick += new EventHandler(PositionCommCheck_Tick);
            PositionCommCheck.Start();
        }

        public void SetMainWindow(MainControlWindow window)
        {
            mainWindow = window;
        }
        
        void PositionCommCheck_Tick(object sender, EventArgs e)
        {
            //this.PositionLabel.Content = mainWindow.ParsePosition();
            PositionLabel.Content = mainWindow.ParsePosition();

            TimeSpan navigationResponseTime = (DateTime.Now - PositionInputHandler.lastCommTime);
            double timeElapsed = navigationResponseTime.TotalSeconds;

            if (timeElapsed <= 1)
            {
                PositionLabel.Background = Brushes.Green;
                PositionLabel.Foreground = Brushes.LightGreen;
            }
            else if (timeElapsed > 1 && timeElapsed < 5)
            {
                PositionLabel.Background = Brushes.DarkOrange;
                PositionLabel.Foreground = Brushes.Black;
            }
            else if (timeElapsed > 10)
            {
                PositionLabel.Background = Brushes.Red;
                PositionLabel.Foreground = Brushes.White;
            }
        }

        private string GetText(RichTextBox richTextBox)
        {
            //use a TextRange to get out the Text from the Document
            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);
            return textRange.Text;
        }

        public void SetImage(System.Windows.Media.ImageSource img, DateTime lastGrabTime, string PositionStr)
        {
            annotationImage.image.Source = img;
            timeOfDay = DateTimeFormat.txt(lastGrabTime);
            GrabTime = lastGrabTime;

            if (PositionStr == null)
                position = "Position: N/A";
            else
                position = PositionStr;
       
            AnnotationSavedIndicator.Visibility = Visibility.Hidden;
        }

        public void SetImageParameters(DateTime lastGrabTime)
        {
            string time = lastGrabTime.ToString("MM/dd/yyyy HH:mm:ss") + " UTC";

            TimeLabel.Content = "Image Capture Time: " + time;
            GrabTime = lastGrabTime;
        }

/*        public void SetITCP(int h, double d, int camTilt, int exposure, int decimation)
        {
            heading = h;
            depth = d;
            cameraTilt = camTilt;
            streamExposure = exposure;
        }
*/
        public void SetIPTC(double depth_local, int tilt_local, int heading_local, int light_local, int cameraTilt_local, int mainTemp_local, int camTemp_local, int mainHum_local, int camHum_local, int roll_local, int forwardBackCruise_local, int leftRightCruise_local, int upDownCruise_local, int mainAft_local, int altForward_local, int altRear_local, int horizForward_local, int horizRear_local, int exposure_local, int decimation_local)
        {
            depth = depth_local;
            tilt = tilt_local;
            heading = heading_local;
            light = light_local;
            cameraTilt = cameraTilt_local;

            mainTemp = mainTemp_local; // main bottle temperature
            camTemp = camTemp_local;  // camera bottle temperature
            mainHum = mainHum_local;  // main bottle humidity
            camHum = camHum_local;  // camera bottle humidity

            roll = roll_local;

            forwardBackCruise = forwardBackCruise_local;
            leftRightCruise = leftRightCruise_local;
            upDownCruise = upDownCruise_local;

            mainAft = mainAft_local;
            altForward = altForward_local;
            altRear = altRear_local;
            horizForward = horizForward_local;
            horizRear = horizRear_local;

            streamExposure = exposure_local;
            decimation = decimation_local;
        }   
        public void SetSnapFullImage(System.Windows.Media.ImageSource img)
        {
            annotationImage.image.Source = img;
            //mainWindow.incrementAnnotatioSerial();
            
            AnnotationSavedIndicator.Content = "Saved: " + mainWindow.getAnnotationSerial();
            AnnotationSavedIndicator.Visibility = Visibility.Visible;         
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            string anno = annotationLongTextCtrl.Text;
            SaveAnnotation(anno);
            //NavCamDisplay.Focus();
        }

        private void SaveButton_RightMouseDown(object sender, MouseButtonEventArgs e)
        {
            string anno = annotationLongTextCtrl.Text;
             
            if (e.RightButton == MouseButtonState.Released)
            {
                SaveAnnotation(anno);
            }
        }
            
        public void SaveAnnotation(string anno)
        {
            string[] path = ApplicationConfig.Path; //alias path directories

            string fname_Image = AnnotationFileName + "_UTC" + ".jpg";
            FileStream stream = new FileStream(path[(int)ApplicationConfig.Path_name.Image] + fname_Image, FileMode.Create); 
            // /*
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            BitmapSource bs = (BitmapSource)annotationImage.image.Source.Clone();

            //add XMP metadata
            BitmapMetadata jpgData = new BitmapMetadata("jpg");
            
            // IPTC header

            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/object name", "<//" + 
            //jpgData.SetQuery("/xmp/dc:source", "<//" +    //ITPC
            
            jpgData.SetQuery("/app1/ifd/{uint=270}","<//" + //EXIF 
            " Location:" + mainWindow.newMissionDialog.GetLocation() +
            " Position:" + position + "\t" +
            " Dive Number:" + mainWindow.newMissionDialog.GetDiveNumber() + 
            " Time In:" + mainWindow.newMissionDialog.GetTimeIn() +
            " Scientist:" + mainWindow.newMissionDialog.GetScientist() +
            " Pilot:" + mainWindow.newMissionDialog.GetPilot() +
            " Vehicle:" + mainWindow.newMissionDialog.GetVehicle() +
 
            " Depth:" + depth.ToString() + 
            " Camera Tilt: " + cameraTilt.ToString() + 
            " Exposure: " + streamExposure.ToString() + 
            " Quality: " + decimation.ToString() + 
            " Annotation: " + annotationLongTextCtrl.Text + "//>");

            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/keywords", )
            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/date created", "3");
            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/time created", "4");
            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/caption", "5");
            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/by-line", "6");
            //jpgData.SetQuery("/app13/irb/8bimiptc/iptc/copyright notice", "7");

            //encoder.Frames.Add(BitmapFrame.Create(bs));
            //encoder.Save(stream);

            encoder.Frames.Add(BitmapFrame.Create(bs, null, jpgData, encoder.ColorContexts));
            encoder.Save(stream);
            // */
            mainWindow.incrementAnnotatioSerial();

            ApplicationConfig.Save();
            AnnotationSavedIndicator.Content = "Saved: " + mainWindow.getAnnotationSerial();
            AnnotationSavedIndicator.Visibility = Visibility.Visible;
            mainWindow.NavCamDisplay.Focus();

            annotationLongTextCtrl.Clear();          
        }

        public string AnnotationFileName {
            get
            {
                string time = "-" + GrabTime.ToString("yyyy_MM_dd_-_H_mm_ss.fff") + "_UTC";
                string fname = DateTimeFormat.file(GrabTime);
                //string serial = mainWindow.getAnnotationSerial().ToString();
                int serialNumber = mainWindow.getAnnotationSerial();

                string serialString;

                if (serialNumber < 10)
                {
                    serialString = "0" + serialNumber.ToString();
                }
                else
                {
                    serialString = serialNumber.ToString();
                }

                
                //return "Annotation," + serial + "," + fname;
                return "Annotation," + "0" + serialString + "-" + time;
            }
        }

        #region Setters for TextDisplays

        public string position
        {
            set
            {
                PositionLabel.Content = value;
            }
            get
            {
                return PositionLabel.Content.ToString();
            }
        }

        public string timeOfDay
        {
            set
            {
                TimeLabel.Content = "Image Capture Time: " + value;
            }
            get
            {
                return TimeLabel.Content.ToString();
            }
        }

        #endregion

        private void Position_Click(object sender, MouseButtonEventArgs e)
        {
            PositionInputSourceConfig w = new PositionInputSourceConfig();
            w.Owner = Application.Current.Windows[0];
            w.serialPortConfig.SetPort(PositionInputHandler.port);
            if (w.ShowDialog() == true)
            {
                //set serial port
                PositionInputHandler.UpdatePort(w.serialPortConfig.SelectedString, Int32.Parse(w.serialPortConfig.BaudRate));
            }

        }

    }
}
