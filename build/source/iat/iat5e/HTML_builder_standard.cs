﻿/*
HTML_builder_standard.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ImageAnnotationTool
{
    static class HTML_builder_standard
    {
        static public string Annotation_html(string ShortAnnotationName,
                                             string ImageFileName,
                                             string SerialNumber,
                                             string Time,
                                             string Position,
                                             string[] AnnotationText,
                                             string AudioFileName,
                                             string OperationName,
                                             string ScientistName)
        {
            string html;

            string fname = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\" +
                           "template.html";

            string formatString;
            StreamReader sr = File.OpenText(fname);
            formatString = sr.ReadToEnd();
            sr.Close();
            string htmlAnnotation = "";
            foreach (string s in AnnotationText)
            {
                htmlAnnotation += s + "<br>";
            }
            html = string.Format(formatString,new object[] {ShortAnnotationName,
                                                            ImageFileName,
                                                            SerialNumber,
                                                            "Time: " + Time,
                                                            "Position: " + Position,
                                                            htmlAnnotation,
                                                            AudioSection(AudioFileName),
                                                            OperationName,
                                                            ScientistName});
            return html;
        }

        public static string AudioSection(string AudioFileName)
        {
            string audioSec = "";
            if (AudioFileName!=null) 
                 audioSec = "<EMBED src=\"" +AudioFileName + "\" AUTOSTART=FALSE LOOP=FALSE width=144 height=20> <br>" + 
		                    "<a href=\""+ AudioFileName + "\"> " + AudioFileName + " </a>";
            return audioSec;

        }
    }
}
