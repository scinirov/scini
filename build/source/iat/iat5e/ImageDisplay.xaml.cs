﻿/*
ImageDisplay.xaml.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageAnnotationTool
{
    public partial class ImageDisplay : UserControl
    {
        string sourceName = "N/A";
        public DateTime lastGrabTime;
        public string PositionStr;

        public ImageDisplay()
        {
            InitializeComponent();
            this.MouseDown += new MouseButtonEventHandler(image_MouseDown);

        }

        private void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.Focus();
        }

        private void image_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            this.Focus();
        }

     
        public string SourceName
        {
            get
            {
                return sourceName;
            }
            set
            {
                sourceName = value;
            }
        }

        public int FPS
        {
            get
            {
                return image.fps;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }

    public class FPSImage : Image
    {
        //public System.Threading.ManualResetEvent renderDone;
        DateTime lasttime;
        public int fps;
        public bool monitorFPS = false;
        public FPSImage()
        {
            //renderDone = new System.Threading.ManualResetEvent(false);
            lasttime = DateTime.Now;
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);
            //renderDone.Set();
            if (monitorFPS)
            {
                TimeSpan ts = DateTime.Now - lasttime;
                lasttime = DateTime.Now;
                if (ts.Milliseconds > 0)
                {
                    fps = (1000 / ts.Milliseconds);
                }
                Console.WriteLine(fps);
            }
        }
    }

}
