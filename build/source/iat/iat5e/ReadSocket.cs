﻿/*
ReadPacket.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace ImageAnnotationTool
{
    public partial class ReadSocket
    {
        private Socket sock = null;

        public void connectToSocket()
        {
            
            IPAddress host = IPAddress.Parse("127.0.0.1");
            IPEndPoint hostep = new IPEndPoint(host, 50548);
            sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                sock.Bind(hostep);
                //sock.Connect(hostep);
                Console.WriteLine("Connected to Socket!");
            }     
                catch (SocketException e) {
                Console.WriteLine("Problem connecting to host");
                Console.WriteLine(e.ToString());
                sock.Close();
                return;
            }
            //sock.Close();
        }
        public void ReadData()
        {
            Byte[] bytesReceived = new Byte[8];
            int bytes = 0;
            sock.Listen(10);
            Socket connection = sock.Accept();
            string result = String.Empty;

            do
            {
                // read the contents of the
                // recieved data into our
                // buffer.

                bytes = connection.Receive(
                    bytesReceived,
                    bytesReceived.Length,
                    0);

                result += Encoding.UTF8.GetString(
                    bytesReceived,
                    0,
                    bytes);


                // continue writing to the buffer 
                // while there's data to be read 
                // from the socket

            } while (connection.Available > 0);

            // output the result to the console
            Console.WriteLine(result);
            Console.WriteLine("done!");


            // we've finnished reading from
            // the socket.
            connection.Close();
        }
    }
}
