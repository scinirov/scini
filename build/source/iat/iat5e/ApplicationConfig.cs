﻿/*
ApplicatoinConfig.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace ImageAnnotationTool
{
    [Serializable()]
    [XmlRoot("ImageAnnotationTool_Settings_Data")]
    public class ApplicationConfigData {

         public string CameraAddress;
         public string UserName;
         public string OperationName;
         public string PositionCommConnection;
         public int PositionCommBaudRate;
         public string PathDataFiles;
         public long AnnotationSerialNumber;
         public bool AskInit;

         public bool ShowPilotDisplay;
         public int  CameraCount;
         public bool UseELPHEL;

         public ApplicationConfigData() 
         {
            
            PositionCommConnection = "COM1";
            PositionCommBaudRate = 9600;
            PathDataFiles = "C:\\2010\\";
        }

        public void SetPath(string path)
        {
            PathDataFiles = "C:\\2010\\" + path;
        }
        
        public System.Net.IPAddress cameraIPAddress
        {
            get
            {
                return System.Net.IPAddress.Parse(CameraAddress);
            }
        }
    }
    
    [Serializable()]
    [XmlRoot("ImageAnnotationTool_Settings")]
    static class ApplicationConfig
    {
        public static ApplicationConfigData data;

        static ApplicationConfig()
        {
            data = new ApplicationConfigData();
        }

        static public bool Load()
        {
            bool rval = true;
            StreamReader reader = null;
            string filename = ConfigFileName;
            
            try
            {
                //XmlSerializer serializer = new XmlSerializer(typeof(ApplicationConfigData));
                reader = new StreamReader(filename);
                //ApplicationConfigData setting = (ApplicationConfigData)serializer.Deserialize(reader);
                reader.Close();

                //data = setting;
            }
            catch
            {
                rval = false;
            }
            

            if (reader != null)
                reader.Close();

            return rval;
        }

        static public void Save()
        {
            string filename = ConfigFileName;
            //XmlSerializer serializer = new XmlSerializer(typeof(ApplicationConfigData));
            StreamWriter writer = new StreamWriter(filename);
            //serializer.Serialize(writer, data);
            writer.Close();
        }

        public enum Path_name {DataRoot, Image};
        public static string[] ShortPath = {"..\\", "image\\"};
        public static string[] Path;

        public static void CreatDirectories() {
            Path = new string[2];
            Path[0] = data.PathDataFiles;
            Path[1] = Path[0] + ShortPath[1];
        }

        public static string RelativeFilePathName(Path_name pathtype, string fname)
        {
            return ShortPath[0] + ShortPath[(int)pathtype] + fname;
        }

        public static void ValidateAndCreateDirectories()
        {
            CreatDirectories();

            foreach (string dir in Path)
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
        }

        private static string ConfigFileName
        {
            get
            {
                string dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\";
                return dir + "Application_settings.config";
            }
        }

    }
}
