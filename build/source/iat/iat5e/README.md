This folder contains the source code for the SCINI Image Annotation Tool (IAT). IAT polls the Elphel cameras on SCINI and records individual .jpg images that sit in the Elphel's buffer. IAT connects to the PC Pilot (LabView) software with a socket, and fetches a information about SCINI's current state (depth, tilt, roll, etc.), and annotates this information into each .jpg image's EXIF data. IAT can also connect to a GPS device that produces a NMEA string and record SCINI's position underwater. A Tritech MicronNav USBL system was most often used in this manner.

At present, this software requires .NET 3.5 to be targeted, and several LabView References that are availiable with LabView device drivers.

For licencing information, please refer to LICENCE.txt
