﻿/*
Master.Designer.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



namespace ImageAnnotationTool
{
    partial class Master
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SlaveButton = new System.Windows.Forms.Button();
            this.MasterButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gold;
            this.label1.Location = new System.Drawing.Point(38, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 24);
            this.label1.TabIndex = 2;
            this.label1.Text = "Choose IAT version:";
            // 
            // SlaveButton
            // 
            this.SlaveButton.BackColor = System.Drawing.Color.Silver;
            this.SlaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SlaveButton.Location = new System.Drawing.Point(145, 72);
            this.SlaveButton.Name = "SlaveButton";
            this.SlaveButton.Size = new System.Drawing.Size(89, 33);
            this.SlaveButton.TabIndex = 3;
            this.SlaveButton.Text = "Slave";
            this.SlaveButton.UseVisualStyleBackColor = false;
            this.SlaveButton.Click += new System.EventHandler(this.SlaveButton_Click);
            // 
            // MasterButton
            // 
            this.MasterButton.BackColor = System.Drawing.Color.Silver;
            this.MasterButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MasterButton.Location = new System.Drawing.Point(12, 72);
            this.MasterButton.Name = "MasterButton";
            this.MasterButton.Size = new System.Drawing.Size(89, 33);
            this.MasterButton.TabIndex = 4;
            this.MasterButton.Text = "Master";
            this.MasterButton.UseVisualStyleBackColor = false;
            this.MasterButton.Click += new System.EventHandler(this.MasterButton_Click);
            // 
            // Master
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(246, 123);
            this.Controls.Add(this.MasterButton);
            this.Controls.Add(this.SlaveButton);
            this.Controls.Add(this.label1);
            this.Name = "Master";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Annotation Tool 2010";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SlaveButton;
        private System.Windows.Forms.Button MasterButton;
    }
}
