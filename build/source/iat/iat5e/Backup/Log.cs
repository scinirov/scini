﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ImageAnnotationTool
{

    static class DateTimeFormat
    {
        const string dateTimeFormat = "yyyy-MM-dd H:mm:ss.fff ";
        const string dateTimeFormat_file = "yyyy_MM_dd_H_mm_ss_fff"; //loose non filesystem safe chars
        public static string file(DateTime time)
        {
            return time.ToUniversalTime().ToString(dateTimeFormat_file);
        }
        public static string txt(DateTime time)
        {
            return time.ToUniversalTime().ToString(dateTimeFormat);
        }
    }
}
