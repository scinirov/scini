﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Management;

namespace ImageAnnotationTool
{
    /// <summary>
    /// Interaction logic for SerialPortConfig.xaml
    /// </summary>
    public partial class SerialPortConfig : UserControl
    {
        class NameData
        {
            public string simpleName;
            public string friendlyName;
        };

        DSS.Communication.Serial.Port port;

        public Brush _ColorDesignator;
        public event EventHandler PortChanged;
        public string SelectedString = "None";

        public SerialPortConfig()
        {

            InitializeComponent();

            //Get Simple Names
            string[] simpleNames = System.IO.Ports.SerialPort.GetPortNames();
            
            List<NameData> names = new List<NameData>();

            //See if we can find some friednly names
            ManagementObjectCollection manobjs;
            ManagementObjectSearcher manobSearcher;
            string QueryStr = "SELECT Name FROM Win32_PnPEntity WHERE ClassGUID = '{4D36E978-E325-11CE-BFC1-08002BE10318}'";
            manobSearcher = new ManagementObjectSearcher(QueryStr);
            manobjs = manobSearcher.Get();
            foreach (ManagementObject ManObj in manobjs)
            {
                NameData n = new NameData();
                n.friendlyName = ManObj["Name"].ToString();
                if (n.friendlyName.Contains("COM"))
                    names.Add(n);
            }

            //Remove duplicates
            foreach (string str in simpleNames)
            {

               
                if (!(str.Contains("COM")))
                    continue;

                bool found = false;
                foreach (NameData name in names)
                {
                    if (name.friendlyName.Contains(str))
                    {
                        name.simpleName = str;
                        found = true;
                    }
                }

                if (!found)
                {
                    NameData n = new NameData();
                    n.simpleName = str;
                    n.friendlyName = "Not Available";
                    names.Add(n);
                }
            }

            //build radio boxes
            foreach (NameData portname in names)
            {
                if (true)
                //if (DSS.Communication.Serial.Port.Check(IOConnections.PortnameToID(portname.simpleName)))
                {
                    RadioButton rb = new RadioButton();
                    rb.FontFamily = new FontFamily("Arial");
                    rb.FontWeight = FontWeights.Bold;
                    rb.ToolTip = portname.friendlyName;
                    rb.Content = portname.simpleName;
                    rb.Click += new RoutedEventHandler(PortSelection_click);
                    Ports.Children.Add(rb);
                }
            }

            //setup baudrate list
            foreach (int speed in DSS.Communication.Serial.Port.StdBaudRates) 
                BaudRateBox.Items.Add(speed);
            BaudRateBox.SelectedItem = 4800; //default

        }

        void PortSelection_click(object sender, RoutedEventArgs e)
        {
            if (port != null)
            {
                //set via manager so it can upate config etc.
                RadioButton rb = (RadioButton)sender;
                SelectedString = rb.Content.ToString();
                if (PortChanged != null)
                    PortChanged(rb.Content.ToString(), new EventArgs());
            }
        }

        public void SetPort(DSS.Communication.Serial.Port p)
        {

            if (port != null)
                return;

            port = p;

            BaudRateBox.SelectedItem = p.speed;
            string portname = "COM"+p.id;
            foreach (RadioButton rb in Ports.Children)
            {
                if (rb.Content.ToString() == portname)
                {
                        if (p.Open())
                        {
                            rb.IsEnabled = true;
                            rb.IsChecked = true;
                            SelectedString = rb.Content.ToString();
                        }
                }
            }
        }

 
        private void BaudRateBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (port != null)
            {
                //set via manager so it can upate config etc.
            }
        }

        public void DisablePortSelector(string PortName)
        {
            if (PortName == "Closed")
                return;
            foreach (RadioButton rb in Ports.Children)
            {
                if (rb.Content.ToString() == PortName)
                {
                    if (PortName != "Closed")
                       rb.IsEnabled = false;
                }
                else
                    rb.IsEnabled = true;
            }
        }

        public String PortNameStr
        {
            set
            {
                PortName.Content = value;
            }

            get
            {
                return PortName.Content.ToString();
            }
        }

        public string BaudRate
        {
            set
            {
                BaudRateBox.SelectedItem = Int32.Parse(value);
            }
            get
            {
                return BaudRateBox.SelectedItem.ToString();
            }
        }
     
        public bool BaudRateIsEnabled
        {
            set
            {
                BaudRateBox.IsEnabled = value;
            }
            get
            {
                return BaudRateBox.IsEnabled;
            }
        }

        public Brush ColorDesignator
        {
            set
            {
                _ColorDesignator = value;
            }
            get
            {
                return _ColorDesignator;
            }
        }

    }
}
