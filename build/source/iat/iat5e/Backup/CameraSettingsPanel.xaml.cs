﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageAnnotationTool
{
    public partial class CameraSettingsPanel : UserControl
    {
        CameraSettings cameraSettings;

        public string CameraName
        {
            set
            {
                CameraNameLabel.Content = value;
            }
            get
            {
                return CameraNameLabel.Content.ToString();
            }
        }

        public CameraSettingsPanel()
        {
            InitializeComponent();
        }

        public void SetCameraParamters(CameraSettings cs)
        {
            cameraSettings = cs;
            UpdateControls();
        }

        public void UpdateControls()
        {
            if (cameraSettings != null)
            {
                ExposureSlider.Value = cameraSettings.streamExposure;
                StillExposureSlider.Value = cameraSettings.stillExposure;
                DelaySlider.Value = cameraSettings.delayMS;
                DelayText.Content = DelaySlider.Value.ToString();

                if (cameraSettings.autoSave)
                {
                    AutoSaveImages.IsChecked = true;
                }
                else
                {
                    AutoSaveImages.IsChecked = false;
                }

                if (cameraSettings.streamAuto)
                {
                    StreamAuto.IsChecked = true;

                }

                if (cameraSettings.rawMode)
                {
                    RawJP46.IsChecked = true;
                }
                else
                {
                    RawJP46.IsChecked = false;
                }

                //assumes only radio buttons on panel and text conforms
                //to a convetion (1/d for deci. and #% for quality.
                foreach (UIElement b in DecimationPanel.Children)
                {
                    if (typeof(RadioButton) == b.GetType())
                    {
                        string s = "1";

                        if (cameraSettings.decimation != 1)
                            s = "1/" + cameraSettings.decimation;
                        if (((RadioButton)b).Content.ToString() == s)
                        {
                            ((RadioButton)b).IsChecked = true;
                        }
                    }
                }

                foreach (UIElement b in JpegQualityPanel.Children)
                {
                    if (typeof(RadioButton) == b.GetType())
                    {
                        string s = cameraSettings.jpegQuality + "%";
                        if (((RadioButton)b).Content.ToString() == s)
                        {
                            ((RadioButton)b).IsChecked = true;
                        }
                    }
                }
            }
        }

        private void Header_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
            }
            else
            {
                if (ControlPanel.Visibility == Visibility.Collapsed)
                {
                    ControlPanel.Visibility = Visibility.Visible;
                }
                else
                {
                    ControlPanel.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void Decimation_Click(object sender, RoutedEventArgs e)
        {
            RadioButton b = (RadioButton)sender;
            if (cameraSettings != null)
            {
                string svalue = b.Content.ToString();
                string value = "" + svalue[svalue.Length - 1];
                cameraSettings.decimation = Int32.Parse(value);

                cameraSettings.dirtyFlag = true;
            }
        }

        private void JpegQuality_Click(object sender, RoutedEventArgs e)
        {
            RadioButton b = (RadioButton)sender;
            Console.WriteLine(b.Content.ToString());
            if (cameraSettings != null)
            {
                string value = b.Content.ToString();
                value = value.Remove(value.Length - 1);
                cameraSettings.jpegQuality = Int32.Parse(value);

                cameraSettings.dirtyFlag = true;
            }
        }

        private void ExposureSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double value = ExposureSlider.Value;
            if (ExposureText != null)
            {
                ExposureText.Content = (value).ToString("0");
            }

            if (cameraSettings != null)
            {
                cameraSettings.streamExposure = (int)(value);
                cameraSettings.dirtyFlag = true;
            }
        }

        private void StillSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (cameraSettings != null)
            {
                cameraSettings.dirtyFlag = true;
                double value = StillExposureSlider.Value;

                if (StillExposureText != null)
                {
                    StillExposureText.Content = (value).ToString("0");
                }

                if (cameraSettings != null)
                {
                    cameraSettings.stillExposure = (int)(value);
                }
            }
        }

        private void DelaySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int value = (int)DelaySlider.Value;
            if (DelayText != null)
            {
                DelayText.Content = value;
            }

            if (cameraSettings != null)
            {
                cameraSettings.delayMS = (value);
                cameraSettings.dirtyFlag = true;
            }
        }

        private void AutoSaveImages_Clicked(object sender, RoutedEventArgs e)
        {
            cameraSettings.autoSave = AutoSaveImages.IsChecked == true;
            cameraSettings.dirtyFlag = true;
        }

        private void RawJP46_Clicked(object sender, RoutedEventArgs e)
        {
            cameraSettings.rawMode = RawJP46.IsChecked == true;
            cameraSettings.dirtyFlag = true;
        }

        private void StreamAuto_Clicked(object sender, RoutedEventArgs e)
        {
            cameraSettings.streamAuto = StreamAuto.IsChecked == true;
            cameraSettings.dirtyFlag = true;
        }
    }
}
