﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageAnnotationTool
{
    public partial class PilotDisplay : Window
    {
        private double sciniDepth = 0;
        private int sciniTilt = 0;
        private int sciniHeading = 0; 
        private int sciniLights = 0;

        private string pilotName;
        private string diveNum;
        private MainControlWindow mainControlWindow;

        public PilotDisplay(MainControlWindow mainWindow)
        {
            InitializeComponent();

            this.KeyDown += new KeyEventHandler(Pilot_KeyDown);
            mainControlWindow = mainWindow;
        }

        public void SetLights(int light)
        {
            sciniLights = light;
            LightLabel.Content = "Lights: " + light.ToString() + "%";
        }

        private void Pilot_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                mainControlWindow.TakeSnapFull();
            }
        }

        public void SetPilotName(string name)
        {
            pilotName = name;
            PilotLabel.Text = "Pilot: " + name;
        }

        public void SetDiveNum(string num)
        {
            diveNum = num;
            DiveLabel.Text = "Dive: " + num;
        }

        public void SetDepth(double depth)
        {
            sciniDepth = depth;
            DepthLabel.Content = "Depth: " + sciniDepth.ToString();
        }

        public void SetTilt(int tilt)
        {
            sciniTilt = tilt;
            TiltLabel.Content = "Tilt: " + sciniTilt.ToString();
        }
        
        public void SetHeading(int heading)
        {
            sciniHeading = heading;
        }
    }
}
