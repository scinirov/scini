﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ImageAnnotationTool
{
    [Serializable()]
    [XmlRoot("ImageAnnotationTool_Camera_Settings")]
    public class CameraSettings
    {
        public int streamExposure = 30; 
        public int stillExposure = 30;
        public int decimation = 4;
        public int jpegQuality = 90;
        public int delayMS = 1000;
        public int bin = 1;
        public double gamma = 1;
        public double saturation = 1;

        public bool dirtyFlag = false;
        public bool snapFull = false;
        public bool rawMode = false;
        public bool autoSave = false;
        public bool streamAuto = false;
    }
}
