﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace ImageAnnotationTool
{
    [Serializable()]
    [XmlRoot("ImageAnnotationTool_Settings_Data")]
    public class ApplicationConfigData {

         public string CameraAddress;
         public string UserName;
         public string OperationName;
         public string PositionCommConnection;
         public int PositionCommBaudRate;
         public string PathDataFiles;
         public long AnnotationSerialNumber;
         public bool AskInit;

         public bool ShowPilotDisplay;
         public int  CameraCount;
         public bool UseELPHEL;

         public ApplicationConfigData() 
         {
            
            PositionCommConnection = "COM1";
            PositionCommBaudRate = 9600;
            PathDataFiles = "C:\\2010\\";
        }

        public void SetPath(string path)
        {
            PathDataFiles = "C:\\2010\\" + path;
        }
        
        public System.Net.IPAddress cameraIPAddress
        {
            get
            {
                return System.Net.IPAddress.Parse(CameraAddress);
            }
        }
    }
    
    [Serializable()]
    [XmlRoot("ImageAnnotationTool_Settings")]
    static class ApplicationConfig
    {
        public static ApplicationConfigData data;

        static ApplicationConfig()
        {
            data = new ApplicationConfigData();
        }

        static public bool Load()
        {
            bool rval = true;
            StreamReader reader = null;
            string filename = ConfigFileName;
            
            try
            {
                //XmlSerializer serializer = new XmlSerializer(typeof(ApplicationConfigData));
                reader = new StreamReader(filename);
                //ApplicationConfigData setting = (ApplicationConfigData)serializer.Deserialize(reader);
                reader.Close();

                //data = setting;
            }
            catch
            {
                rval = false;
            }
            

            if (reader != null)
                reader.Close();

            return rval;
        }

        static public void Save()
        {
            string filename = ConfigFileName;
            //XmlSerializer serializer = new XmlSerializer(typeof(ApplicationConfigData));
            StreamWriter writer = new StreamWriter(filename);
            //serializer.Serialize(writer, data);
            writer.Close();
        }

        public enum Path_name {DataRoot, Image};
        public static string[] ShortPath = {"..\\", "image\\"};
        public static string[] Path;

        public static void CreatDirectories() {
            Path = new string[2];
            Path[0] = data.PathDataFiles;
            Path[1] = Path[0] + ShortPath[1];
        }

        public static string RelativeFilePathName(Path_name pathtype, string fname)
        {
            return ShortPath[0] + ShortPath[(int)pathtype] + fname;
        }

        public static void ValidateAndCreateDirectories()
        {
            CreatDirectories();

            foreach (string dir in Path)
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
        }

        private static string ConfigFileName
        {
            get
            {
                string dir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\";
                return dir + "Application_settings.config";
            }
        }

    }
}
