﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net;
using System.Configuration;
using System.Reflection;
using System.Timers;

namespace ImageAnnotationTool
{
    class ImageDisplayActionParameter
    {
        public MemoryStream ms;
        public ImageDisplay display;

        public ImageDisplayActionParameter(MemoryStream m, ImageDisplay img)
        {
            ms = m;
            display = img;
        }
    }

    class ElphelCamera
    {
        public ImageDisplay display;
        public CameraSettings parameters;
        public UInt64 imgCnt;
        public DateTime lastGrabTime;
        public string PositionStr;

        public ElphelCamera(ImageDisplay i, CameraSettings cs)
        {
            display = i;
            parameters = cs;
            imgCnt = 0;
            PositionStr = "";
        }
    }

    class ElphelCameraHttpDriver
    {
        System.Windows.Threading.Dispatcher Dispatcher;

        public enum displayId { Nav = 0, Pilot = 1 };

        private ElphelCamera[] camera = null;

        public ImageDisplay pilotDisplayTarget;

        private string ipAddress;               //elphel camera ip
        private string imageServerRequest;      //ip address to grab camera frame
        private string snapFullServerRequest;   //ip address to grab high res camera frame
        private string EXIFDescriptionRequest;
        private string PHPServer;               //ip address of elphel PHP server      

        private bool snapFull = false;
        private bool autoAquire = false;

        private BitmapImage biFull;
        private DateTime lastGrabTime;
        private AnnotationControl annotationControl;
        private MainControlWindow mainWindow;

        double depth = 0;
        int tilt = 0;
        int heading = 0;
        int light = 0;
        int cameraTilt = 0;

        int mainTemp = 0; // main bottle temperature
        int camTemp = 0;  // camera bottle temperature
        int mainHum = 0;  // main bottle humidity
        int camHum = 0;   // camera bottle humidity

        int roll = 0;

        int forwardBackCruise = 0;
        int leftRightCruise = 0;
        int upDownCruise = 0;

        int mainAft = 0;
        int altForward = 0;
        int altRear = 0;
        int horizForward = 0;
        int horizRear = 0;

        private TextWriter csvWriter;
        private long imageSequence = 0;

        public ElphelCameraHttpDriver(string ip, AnnotationControl anno, MainControlWindow main)
        {
            imageSequence = 0;
            SetAllowUnsafeHeaderParsing20();

            ipAddress = ip;
            imageServerRequest = "http://" + ipAddress + ":8081/towp/wait/img";
            snapFullServerRequest = "http://" + ipAddress + "/snapfull.php";
            EXIFDescriptionRequest = "http://" + ipAddress + "/setexif.php?description=";
            PHPServer = "http://" + ipAddress + "/";

            annotationControl = anno;
            mainWindow = main;

            csvWriter = new StreamWriter(ApplicationConfig.Path[1] + "csv.txt", true); //write csv header 

            //columns with more than one word should use underscores instead of spaces between words to assist ArcGIS.
            string write =
            "Image" + "\t" +
            "Time_UTC" + "\t" +
            "Location" + "\t" +
            "B1E" + "\t" +
            "B1N" + "\t" +
            "B2E" + "\t" +
            "B2N" + "\t" +
            "Position" + "\t" +
            "Position_Age_sec" + "\t" +
            "Dive_Number" + "\t" +
            "Time_In_UTC" + "\t" +
            "Scientist" + "\t" +
            "Pilot" + "\t" +
            "Vehicle" + "\t" +
            "Depth" + "\t" +
            "Tilt" + "\t" +
            "Heading" + "\t" +
            "Light" + "\t" +
            "Camera_Tilt" + "\t" +
            "Main_Temp" + "\t" +
            "Cam_Temp" + "\t" +
            "Main_Hum" + "\t" +
            "Cam_Hum" + "\t" +
            "Roll" + "\t" +
            "ForwardBackCruise" + "\t" +
            "LeftRightCruise" + "\t" +
            "UpDownCruise" + "\t" +
            "MainAft" + "\t" +
            "AltForward" + "\t" +
            "AltRear" + "\t" +
            "HorizForward" + "\t" +
            "HorizRear" + "\t" +
            "Exposure" + "\t" +
            "Quality" + "\t";
            //"Annotation";

            csvWriter.WriteLine(write, FileMode.Append);
            csvWriter.Close();
        }

        public void SetPCPilotData(double depth_local, int tilt_local, int heading_local, int light_local, int cameraTilt_local, int mainTemp_local, int camTemp_local, int mainHum_local, int camHum_local, int roll_local, int forwardBackCruise_local, int leftRightCruise_local, int upDownCruise_local, int mainAft_local, int altForward_local, int altRear_local, int horizForward_local, int horizRear_local)
        {
            depth = depth_local;
            tilt = tilt_local;
            heading = heading_local;
            light = light_local;
            cameraTilt = cameraTilt_local;
            
            mainTemp = mainTemp_local; // main bottle temperature
            camTemp = camTemp_local;  // camera bottle temperature
            mainHum = mainHum_local;  // main bottle humidity
            camHum = camHum_local;  // camera bottle humidity

            roll = roll_local;

            forwardBackCruise = forwardBackCruise_local;
            leftRightCruise = leftRightCruise_local;
            upDownCruise = upDownCruise_local;

            mainAft = mainAft_local;
            altForward = altForward_local;
            altRear = altRear_local;
            horizForward = horizForward_local;
            horizRear = horizRear_local;

        }

        public void SetSnapFull(bool saved)
        {
            snapFull = saved;
        }

        public void SetAutoAquire(bool aquire)
        {
            autoAquire = aquire;
        }
        public void SetDisplays(ImageDisplay Nav, CameraSettings NavParams, ImageDisplay Pilot)
        {
            camera = new ElphelCamera[2];
            camera[0] = new ElphelCamera(Nav, NavParams);
            camera[1] = new ElphelCamera(Pilot, NavParams);
            pilotDisplayTarget = Nav; //set pilot display to motion camera
        }

        public BitmapImage getSnapFull()
        {
            return biFull;
        }

        void Draw(ImageDisplayActionParameter param)
        {
            try
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.StreamSource = param.ms;
                bi.EndInit();

                param.display.image.Source = bi;

                if (autoAquire)
                {
                    biFull = bi;
                    annotationControl.SetSnapFullImage(biFull);
                    lastGrabTime = DateTime.Now;
                    snapFull = false;
                }
                else
                {
                    if (param.display == pilotDisplayTarget)
                    {
                        camera[(int)displayId.Pilot].display.image.Source = bi;
                    }
                }

            }
            catch (Exception e)
            {
                System.Windows.MessageBox.Show(e.ToString(), "Error drawing image.");
            }
        }

        public string ParametersToPHPCommand(CameraSettings parameters)
        {   
            string command;

            if (parameters.streamAuto)  //use elphel autoexposure
            {
                //command = String.Format("camvc.php?set=0/comp_run:stop/ae:on/iq:{0}/dh:{1}/dv:{1}/comp_run:run",
                // Change decimation (dh/dv) to decimation and binning (bh/bv).
                command = String.Format("camvc.php?set=0/comp_run:stop/ae:on/iq:{0}/bh:{1}/bv:{1}/dh:{1}/dv:{1}/comp_run:run",
                parameters.jpegQuality, parameters.decimation);
            }
            else
            {
                //command = String.Format("camvc.php?set=0/comp_run:stop/ae:off/iq:{0}/e:{1}/dh:{2}/dv:{2}/comp_run:run",
                // Change decimation (dh/dv) to decimation and binning (bh/bv).
                command = String.Format("camvc.php?set=0/comp_run:stop/ae:off/iq:{0}/e:{1}/bh:{2}/bv:{2}/dh:{2}/dv:{2}/comp_run:run",
                parameters.jpegQuality, parameters.streamExposure, parameters.decimation);
            }

            string rval = PHPServer + command;
            return rval;
        }

        public void GrabImage(ElphelCamera grab_camera)
        {

            imageSequence++;
            string imageSequenceString = "";
            string name = "";
                    
            if (imageSequence < 10)
            {
                imageSequenceString = "0" + imageSequence.ToString();
            }
            else
            {
                imageSequenceString = imageSequence.ToString();
            }

            try
            {
                if (grab_camera.parameters.dirtyFlag == true) //if new camera parameters are available
                {
                    DoHttpRequest(ParametersToPHPCommand(grab_camera.parameters)); //send parameters
                    grab_camera.parameters.dirtyFlag = false;  //image has been updated
                    autoAquire = grab_camera.parameters.autoSave;
                }

                grab_camera.lastGrabTime = DateTime.Now; //.AddHours(1);
                grab_camera.PositionStr = PositionInputHandler.PositionData; //get position
                grab_camera.display.lastGrabTime = grab_camera.lastGrabTime;
                grab_camera.display.PositionStr = grab_camera.PositionStr;

                if (grab_camera.PositionStr == null)
                {
                    grab_camera.PositionStr = "None";
                }

                if (snapFull)
                {
                    string serialString;
                    int serialNumber = mainWindow.getAnnotationSerial();

                    if (serialNumber < 10)
                    {
                        serialString = "0" + serialNumber.ToString();
                    }
                    else
                    {
                        serialString = serialNumber.ToString();
                    }

                    name = "Still," + imageSequence.ToString() + "-0" + serialString;
                }
                else
                {
                    name = "Stream," + imageSequence.ToString();
                }

                //string time = "-" + grab_camera.lastGrabTime.ToString("yyyy_MM_dd_H_mm_ss_fff") + "_UTC";

                string time = "-" + grab_camera.lastGrabTime.ToString("yyyy_MM_dd_-_H_mm_ss.fff") + "_UTC";

                string fname = name + "," + time;
                
                string fname_Image = fname + ".jpg";

                TimeSpan navigationResponseTime = (DateTime.Now - PositionInputHandler.lastCommTime);
                string timeElapsed = navigationResponseTime.TotalSeconds.ToString();

                string write_csv =

                    fname_Image + "\t" +
                    time + "\t" +
                    mainWindow.newMissionDialog.GetLocation() + "\t" +
                    mainWindow.newMissionDialog.GetB1E() + "\t" +
                    mainWindow.newMissionDialog.GetB1N() + "\t" +
                    mainWindow.newMissionDialog.GetB2E() + "\t" +
                    mainWindow.newMissionDialog.GetB2N() + "\t" +
                    grab_camera.PositionStr + "\t" +
                    timeElapsed + "\t" +
                    mainWindow.newMissionDialog.GetDiveNumber() + "\t" +
                    mainWindow.newMissionDialog.GetTimeIn() + "\t" +
                    mainWindow.newMissionDialog.GetScientist() + "\t" +
                    mainWindow.newMissionDialog.GetPilot() + "\t" +
                    mainWindow.newMissionDialog.GetVehicle() + "\t" +
                    depth.ToString() + "\t" +
                    tilt.ToString() + "\t" +
                    heading.ToString() + "\t" +
                    light.ToString() + "\t" +
                    cameraTilt.ToString() + "\t" +
                    mainTemp.ToString() + "\t" +
                    camTemp.ToString() + "\t" +
                    mainHum.ToString() + "\t" +
                    camHum.ToString() + "\t" +
                    roll.ToString() + "\t" +
                    forwardBackCruise.ToString() + "\t" +
                    leftRightCruise.ToString() + "\t" +
                    upDownCruise.ToString() + "\t" +
                    mainAft.ToString() + "\t" +
                    altForward.ToString() + "\t" +
                    altRear.ToString() + "\t" +
                    horizForward.ToString() + "\t" +
                    horizRear.ToString() + "\t" +
                    grab_camera.parameters.streamExposure.ToString() + "\t" +
                    grab_camera.parameters.decimation.ToString() + "\t";
                    //" Annotation: " + mainWindow.annotationControl.annotationLongTextCtrl.Text.ToString();

                string write_exif =

                    "NAME:" + fname_Image + "\t" +
                    " TIME_UTC:" + time + "\t" +
                    " LOC:" + mainWindow.newMissionDialog.GetLocation() + "\t" +
                    " B1E:" + mainWindow.newMissionDialog.GetB1E() + "\t" +
                    " B1N:" + mainWindow.newMissionDialog.GetB1N() + "\t" +
                    " B2E:" + mainWindow.newMissionDialog.GetB2E() + "\t" +
                    " B2N:" + mainWindow.newMissionDialog.GetB2N() + "\t" +
                    " POSITION:" + grab_camera.PositionStr + "\t" +
                    " POS_AGE_SEC:" + timeElapsed + "\t" +
                    " DIVE_NO:" + mainWindow.newMissionDialog.GetDiveNumber() + "\t" +
                    " TIME_IN_UTC:" + mainWindow.newMissionDialog.GetTimeIn() + "\t" +
                    " SCI:" + mainWindow.newMissionDialog.GetScientist() + "\t" +
                    " PILOT:" + mainWindow.newMissionDialog.GetPilot() + "\t" +
                    " VEH:" + mainWindow.newMissionDialog.GetVehicle() + "\t" +
                    " DEPTH:" + depth.ToString() + "\t" +
                    " TILT:" + tilt.ToString() + "\t" +
                    " HEADING:" + heading.ToString() + "\t" +
                    " LIGHT:" + light.ToString() + "\t" +
                    " CAM_TILT:" + cameraTilt.ToString() + "\t" +
                    " MAIN_TEMP:" + mainTemp.ToString() + "\t" +
                    " CAM_TEMP:" + camTemp.ToString() + "\t" +
                    " MAIN_HUM:" + mainHum.ToString() + "\t" +
                    " CAM_HUM:" + camHum.ToString() + "\t" +
                    " ROLL:" + roll.ToString() + "\t" +
                    " FWDBACKCRUISE:" + forwardBackCruise.ToString() + "\t" +
                    " LRCRUISE:" + leftRightCruise.ToString() + "\t" +
                    " UDCRUISE:" + upDownCruise.ToString() + "\t" +
                    " MAINAFT:" + mainAft.ToString() + "\t" +
                    " ALTFWD:" + altForward.ToString() + "\t" +
                    " ALTREAR:" + altRear.ToString() + "\t" +
                    " HORIZFWD:" + horizForward.ToString() + "\t" +
                    " HORIZREAR:" + horizRear.ToString() + "\t" +
                    " EXPOSURE:" + grab_camera.parameters.streamExposure.ToString() + "\t" +
                    " DECIMATION:" + grab_camera.parameters.decimation.ToString() + "\t";
                    //"Annotation: " + mainWindow.annotationControl.annotationLongTextCtrl.Text.ToString();

                byte[] imageBuffer = null;
                byte[] EXIFbuffer = null;

                if (autoAquire)
                {
                    string command = String.Format("?EXPOS={0}&COLOR=1", grab_camera.parameters.stillExposure * 1000);
                    
                    if (grab_camera.parameters.rawMode)
                    {
                        command = String.Format("?COLOR=2");
                    }
                    string snapFullRequest = snapFullServerRequest + command;
                    
                    EXIFbuffer = DoHttpRequest(EXIFDescriptionRequest + write_exif); //add EXIF header information in Elphel
                    imageBuffer = DoHttpRequest(snapFullRequest); //grab full resolution snapshot image and place in buffer 
                }
                else
                {
                    EXIFbuffer = DoHttpRequest(EXIFDescriptionRequest + write_exif); //add EXIF header information in Elphel
                    imageBuffer = DoHttpRequest(imageServerRequest); //grab image and place in buffer 
                }

                if (imageBuffer != null) //if image server request returned an image
                { 
                    Action<ImageDisplayActionParameter> action = Draw;
                    MemoryStream ms = new MemoryStream(imageBuffer);
                    ImageDisplayActionParameter param = null;

                    FileStream stream = new FileStream(ApplicationConfig.Path[(int)ApplicationConfig.Path_name.Image] + fname_Image,
                                                           FileMode.Create);

                    stream.Write(imageBuffer, 0, imageBuffer.Length); //write the image to a file
                    stream.Close();
                    
                    csvWriter = new StreamWriter(ApplicationConfig.Path[1] + "csv.txt",true); //write the csv file
                    csvWriter.WriteLine(write_csv,FileMode.Append);
                    csvWriter.Close();
                   
                    param = new ImageDisplayActionParameter(ms, grab_camera.display);

                    if (!this.Dispatcher.CheckAccess())
                    {
                        //execute delegate synchronously in an attempt to minimize latency
                        this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.ContextIdle, action, param);
                    }
                    else
                    {
                        action(param); //draw parameter
                    }
                }
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.ToString(), "Thread Error.");
            }
        }

        #region Acquisition Thread

        public bool runThread = false;

        void CameraControlThread()
        {
            bool detectDone = false;
            int count = 0;

            //for detection just spin until we see a response from the image server.
            while (!detectDone)
            {
                byte[] buffer = DoHttpRequest(imageServerRequest);
                if (buffer != null)
                {
                    detectDone = true;
                }
                count++;
                if (count > 1)
                {
                    if (MessageBox.Show("Could not detect Elphel camera at " + ipAddress.ToString() + "\r\n" + "Would you like to retry?", "Connection Error", MessageBoxButton.YesNo, MessageBoxImage.Error) == MessageBoxResult.Yes)
                    {
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
            }

            while (runThread)
            {
                foreach (ElphelCamera cam in camera)
                {

                if (camera[0].parameters.autoSave)
                {
                    if (((DateTime.Now - lastGrabTime).TotalMilliseconds >= camera[0].parameters.delayMS))
                    {
                        autoAquire = true;
                    }
                    else
                    {
                        autoAquire = false;
                    }
                }
                else if (snapFull)
                {
                    autoAquire = true;
                }
                else
                {
                    autoAquire = false;
                }
                GrabImage(cam);

                }
            }
        }

        public void StartThread(System.Windows.Threading.Dispatcher uiDispatcher)
        {
            Dispatcher = uiDispatcher;
            runThread = true;
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(this.CameraControlThread));
            thread.Priority = System.Threading.ThreadPriority.Lowest;
            thread.Start();
        }

        public void EndThread()
        {
            runThread = false;
        }
        #endregion

        #region Special Helper Functions

        public byte[] DoHttpRequest(string url)
        {
            return DoHttpRequest(url, false, 1000);
        }

        public byte[] DoHttpRequest(string url, bool wantNonImageReplys, int timeout)
        {
            HttpWebRequest myReq = null;

            try
            {
                myReq = (HttpWebRequest)WebRequest.Create(url);
                myReq.Timeout = timeout;
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message, "Connection Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
            WebResponse resp = null;

            try
            {
                resp = myReq.GetResponse();
            }
            catch (Exception exp)
            {
                Console.WriteLine("Err: " + url + " : " + exp.ToString());
            }

            if (resp != null)
            {
                byte[] buffer;
                if (resp.ContentLength > 0)
                {
                    buffer = new byte[resp.ContentLength];
                    ReadWholeArray(resp.GetResponseStream(), buffer);
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    string str = encoding.GetString(buffer);

                    resp.Close();
                }
                else
                {
                    if (!wantNonImageReplys)
                    {
                        buffer = null;
                    }
                    else
                    {
                        //print out replys from php scripts to console if desired.
                        System.IO.StreamReader sr = new
                        System.IO.StreamReader(resp.GetResponseStream(), System.Text.Encoding.ASCII);

                        string sa = sr.ReadToEnd();
                        sr.Close();
                        System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
                        buffer = encoding.GetBytes(sa);
                    }
                    resp.Close();
                }
                return buffer;
            }
            return null;
        }

        //reads data into a complete array, throwing an EndOfStreamException if the stream runs out of data first,
        //or if an IOException naturally occurs.
        //<param name="stream">The stream to read data from</param>
        //<param name="data">The array to read bytes into. 
        //The array will be completely filled from the stream, so an appropriate size must be given.
        public static void ReadWholeArray(Stream stream, byte[] data)
        {
            int offset = 0;
            int read = 0; 
            int remaining = data.Length;
            while (remaining > 0)
            {
                try
                {
                    read = stream.Read(data, offset, remaining);
                }
                catch
                {
                    MessageBox.Show("Elphel connection closed, cannot connect to camera.", "Connection Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                
                if (read <= 0)
                throw new EndOfStreamException
                        (String.Format("End of stream reached with {0} bytes left to read", remaining));
                remaining -= read;
                offset += read;
            }
        }

        private static bool SetAllowUnsafeHeaderParsing20()
        {
            //Get the assembly that contains the internal class
            Assembly aNetAssembly = Assembly.GetAssembly(typeof(System.Net.Configuration.SettingsSection));
            if (aNetAssembly != null)
            {
                //Use the assembly in order to get the internal type for the internal class
                Type aSettingsType = aNetAssembly.GetType("System.Net.Configuration.SettingsSectionInternal");
                if (aSettingsType != null)
                {
                    //Use the internal static property to get an instance of the internal settings class.
                    //If the static instance isn't created allready the property will create it for us.
                    object anInstance = aSettingsType.InvokeMember("Section",
                      BindingFlags.Static | BindingFlags.GetProperty | BindingFlags.NonPublic, null, null, new object[] { });

                    if (anInstance != null)
                    {
                        //Locate the private bool field that tells the framework is unsafe header parsing should be allowed or not
                        FieldInfo aUseUnsafeHeaderParsing = aSettingsType.GetField("useUnsafeHeaderParsing", BindingFlags.NonPublic | BindingFlags.Instance);
                        if (aUseUnsafeHeaderParsing != null)
                        {
                            aUseUnsafeHeaderParsing.SetValue(anInstance, true);
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        #endregion
    }
}
