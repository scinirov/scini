﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageAnnotationTool
{
    public partial class Master : Form
    {
        bool isMaster;

        public Master()
        {
            InitializeComponent();
        }

        public bool IsMaster()
        {
            return isMaster;
        }
        private void MasterButton_Click(object sender, EventArgs e)
        {
            isMaster = true;
            this.Close();
        }

        private void SlaveButton_Click(object sender, EventArgs e)
        {
            isMaster = false;
            this.Close();
        }

        
    }
}
