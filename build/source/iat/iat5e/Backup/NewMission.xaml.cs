﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace ImageAnnotationTool
{
    [Serializable]

    public class missionData
    {
        public string location;
        public string vehicle;
        public string cameraIP;
        public string pilotName;
        public string scientistName;
        public string diveNumber;
        public string timeIn;
        public string b1E;
        public string b1N;
        public string b2E;
        public string b2N;
        public string gpsDevice;
        public string navDevice;
    }

    public partial class NewMission : Window
    {
        missionData missionData;
        private bool isClosed = false;

        public NewMission()
        {
            missionData = new missionData();

            missionData.location = "N/A";
            missionData.diveNumber = "N/A";
            missionData.timeIn = "N/A";
            missionData.vehicle = "N/A";
            missionData.cameraIP = "127.0.0.0";
            missionData.pilotName = "N/A";
            missionData.scientistName = "N/A";
            missionData.b1E = "N/A";
            missionData.b1N = "N/A";
            missionData.b2E = "N/A";
            missionData.b2N = "N/A";
            missionData.gpsDevice = "N/A";
            missionData.navDevice = "N/A";

            InitializeComponent();

            VehicleListBox.SelectedIndex = 0;
            IPListBox.SelectedIndex = 0;
            NavDeviceBox.SelectedIndex = 0;

            LocationBox.Text = missionData.location;
            ScientistBox.Text = missionData.scientistName;
            DiveBox.Text = missionData.diveNumber;
            TimeBox.Text = missionData.timeIn;
            PilotBox.Text = missionData.pilotName;
        }

        public bool getIsClosed()
        {
            return isClosed;
        }

        public string GetPilot() { return missionData.pilotName; }
        public string GetScientist() { return missionData.scientistName; }
        public string GetDiveNumber() { return missionData.diveNumber; }
        public string GetTimeIn() { return missionData.timeIn; }
        public string GetLocation() { return missionData.location; }
        public string GetVehicle() { return missionData.vehicle; }
        public string GetCameraIP() { return missionData.cameraIP; }
        public string GetB1E() { return missionData.b1E; }
        public string GetB1N() { return missionData.b1N; }
        public string GetB2E() { return missionData.b2E; }
        public string GetB2N() { return missionData.b2N; }
        public string GetGPSDevice() { return missionData.gpsDevice; }
        public string GetNavDevice() { return missionData.navDevice; }

        public void SetPilot(string pilot) { missionData.pilotName = pilot; PilotBox.Text = missionData.pilotName; }
        public void SetScientist(string scientist) { missionData.scientistName = scientist; ScientistBox.Text = missionData.scientistName; }
        public void SetDiveNumber(string dive) { missionData.diveNumber = dive; DiveBox.Text = missionData.diveNumber; }
        public void SetTimeIn(string time) { missionData.timeIn = time; TimeBox.Text = missionData.timeIn; }
        public void SetLocation(string loc) { missionData.location = loc; LocationBox.Text = missionData.location; }
        public void SetVehicle(string veh) { missionData.vehicle = veh; VehicleListBox.Text = missionData.vehicle; }
        public void SetCameraIP(string camera) { missionData.cameraIP = camera; IPListBox.Text = missionData.cameraIP; }
        public void SetB1E(string b1Easting) { missionData.b1E = b1Easting; B1EBox.Text = missionData.b1E; }
        public void SetB1N(string b1Northing) { missionData.b1N = b1Northing; B1NBox.Text = missionData.b1N; }
        public void SetB2E(string b2Easting) { missionData.b2E = b2Easting; B2EBox.Text = missionData.b2E; }
        public void SetB2N(string b2Northing) { missionData.b2N = b2Northing; B2NBox.Text = missionData.b2N; }
        public void SetGPSDevice(string gps) { missionData.gpsDevice = gps; GPSDeviceBox.Text = missionData.gpsDevice; }
        public void SetNavDevice(string nav) { missionData.navDevice = nav; NavDeviceBox.Text = missionData.navDevice; }

        private void StartIATButton_Click(object sender, RoutedEventArgs e)
        {   
            missionData.location = LocationBox.Text;
            missionData.vehicle = VehicleListBox.Text;
            missionData.cameraIP = IPListBox.Text;
            missionData.pilotName = PilotBox.Text;
            missionData.scientistName = ScientistBox.Text;
            missionData.diveNumber = DiveBox.Text;
            missionData.timeIn = TimeBox.Text;
            missionData.b1E = B1EBox.Text;
            missionData.b1N = B1NBox.Text;
            missionData.b2E = B2EBox.Text;
            missionData.b2N = B2NBox.Text;
            missionData.gpsDevice = GPSDeviceBox.Text;
            missionData.navDevice = NavDeviceBox.Text;

            isClosed = true;
            this.EditWindow();
            this.Hide();

            FileStream fileStream = new FileStream(@"C:\2010\data.txt", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fileStream,missionData);
            fileStream.Close();
        }

        public void EditWindow()
        {
            LocationBox.Text = missionData.location;
            VehicleListBox.Text = missionData.vehicle;
            IPListBox.Text = missionData.cameraIP;
            PilotBox.Text = missionData.pilotName;
            ScientistBox.Text = missionData.scientistName;
            DiveBox.Text = missionData.diveNumber;
            TimeBox.Text = missionData.timeIn;
            VehicleListBox.Text = missionData.vehicle;
            IPListBox.Text = missionData.cameraIP;
            B1EBox.Text = missionData.b1E;
            B1NBox.Text = missionData.b2N;
            B2EBox.Text = missionData.b2E;
            B2NBox.Text = missionData.b2N;
            GPSDeviceBox.Text = missionData.gpsDevice;
            NavDeviceBox.Text = missionData.navDevice;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Closed(object sender, EventArgs e)
        {
        }
    }
}
