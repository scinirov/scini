﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NationalInstruments;
using NationalInstruments.Net;
using NationalInstruments.UI.WindowsForms;
using System.IO.Ports;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

    namespace ImageAnnotationTool
    {
        [Serializable]
        public partial class MainControlWindow : Window
        {
            bool isMaster;
            bool elphelConnected = false;

            private PilotDisplay pilotDisplay;       
            private ElphelCameraHttpDriver cameraDriver;
            public CameraSettings navCameraSettings;
            public NewMission newMissionDialog;
            public Master masterDialog;

            private NationalInstruments.Net.DataSocket readDataSocket;
            private NationalInstruments.Net.DataSocket readMasterDataSocket;
            private NationalInstruments.Net.DataSocket writeDataSocket;
            
            private const string readTargetUrl = "dstp://192.168.2.77/SCINIPILOT"; //PCPilot computer ip address for datasocket
            private const string writeTargetUrl = "dstp://localhost/SCINIAT";
            private const string readMasterTargetUrl = "dstp://192.168.2.77/SCINIAT"; //read from master on pilot computer

            private bool dataSocketAttempted = false;
            private DatasocketProgress dataSocketProgress;
            
            //PCPilot variables to read
            private const string depthAttributeName = "Depth";
            private const string tiltAttributeName = "Tilt";
            private const string headingAttributeName = "Heading";
            private const string lightsAttributeName = "Lights";
            private const string cameraTiltAttributeName = "CameraTilt";
            private const string locationAttributeName = "Location";
            private const string diveAttributeName = "Dive";
            private const string timeInAttributeName = "TimeIn";
            private const string pilotAttributeName = "Pilot";
            private const string scientistAttributeName = "Scientist";
            private const string vehicleAttributeName = "Vehicle";
            private const string cameraAttributeName = "Camera";
            private const string B1EAttributeName = "B1E";
            private const string B1NAttributeName = "B1N";
            private const string B2EAttributeName = "B2E";
            private const string B2NAttributeName = "B2N";
            private const string gpsDeviceAttributeName = "GPSDevice";
            private const string navDeviceAttributeName = "NavDevice";

            private const string mainTempAttributeName = "MainTemp";
            private const string camTempAttributeName = "CamTemp";
            private const string mainHumAttributeName = "MainHum";
            private const string camHumAttributeName = "CamHum";

            private const string rollAttributeName = "Roll";

            private const string forwardBackCruiseAttributeName = "ForwardBackCruise";
            private const string leftRightCruiseAttributeName = "LeftRightCruise";
            private const string upDownCruiseAttributeName = "UpDownCruise";

            private const string mainAftAttributeName = "MainAft";
            private const string altForwardAttributeName = "AltForward";
            private const string altRearAttributeName = "AltRear";
            private const string horizForwardAttributeName = "HorizForward";
            private const string horizRearAttributeName = "HorizRear";

            private double depth = 0;
            private int tilt = 0;       //vehicle tilt
            private int heading = 0;
            private int light = 0;      //percentage of forward light ring on
            private int cameraTilt = 0; //camera tilt angle

            private int mainTemp = 0; // main bottle temperature
            private int camTemp = 0;  // camera bottle temperature
            private int mainHum = 0;  // main bottle humidity
            private int camHum = 0;   // camera bottle humidity

            private int roll = 0;

            private int forwardBackCruise = 0;
            private int leftRightCruise = 0;
            private int upDownCruise = 0;

            private int mainAft = 0;
            private int altForward = 0;
            private int altRear = 0;
            private int horizForward = 0;
            private int horizRear = 0;

            private int annotationSerial = 0;

            private bool laserOverlay = false;

            private string ip; //elphel ip address
           
            List<Key> currentKeyStack = new List<Key>();
            TimeSpan lengthOfTimeForChordStroke = new TimeSpan(0, 0, 0, 0, 100);  //time allowed for double spacebar
            DateTime lastUpdate = DateTime.Now;

            public MainControlWindow()
            {
                InitializeComponent();

                annotationControl.SetMainWindow(this);
                annotationControl.position = "N/A";

                newMissionDialog = new NewMission();
                masterDialog = new Master();

                masterDialog.ShowDialog();
                isMaster = masterDialog.IsMaster();

                missionData loadMission = new missionData();

                FileStream fileStream = null;

                try
                {
                    fileStream = new FileStream(@"C:\2010\data.txt", FileMode.OpenOrCreate);
                    BinaryFormatter bf = new BinaryFormatter();

                    loadMission = (missionData)bf.Deserialize(fileStream);
                    fileStream.Close();
                }
                catch
                {
                    MessageBox.Show("Could not deserialize data.", "Serialization Error");
                    fileStream.Close();
                }
                
                newMissionDialog.SetLocation(loadMission.location);
                newMissionDialog.SetDiveNumber(loadMission.diveNumber);
                newMissionDialog.SetTimeIn(loadMission.timeIn);
                newMissionDialog.SetVehicle(loadMission.vehicle);
                newMissionDialog.SetCameraIP(loadMission.cameraIP);
                newMissionDialog.SetPilot(loadMission.pilotName);
                newMissionDialog.SetScientist(loadMission.scientistName);
                newMissionDialog.SetB1E(loadMission.b1E);
                newMissionDialog.SetB1N(loadMission.b1N);
                newMissionDialog.SetB2E(loadMission.b2E);
                newMissionDialog.SetB2N(loadMission.b2N);
                newMissionDialog.SetGPSDevice(loadMission.gpsDevice);
                newMissionDialog.SetNavDevice(loadMission.navDevice);

                if (isMaster) 
                {
                    newMissionDialog.ShowDialog();
                }

                ip = newMissionDialog.GetCameraIP();
                
                ApplicationConfig.Load();
                ApplicationConfig.data.SetPath(newMissionDialog.GetDiveNumber() + " Site " + newMissionDialog.GetLocation() + "2010" + "\\");
                ApplicationConfig.ValidateAndCreateDirectories();

                navCameraSettings = new CameraSettings();

                try
                {
                    readDataSocket = new NationalInstruments.Net.DataSocket();
                    writeDataSocket = new NationalInstruments.Net.DataSocket();
                    
                    readDataSocket.AutoConnect = false;
                    writeDataSocket.AutoConnect = false;

                    if (!isMaster)
                    {
                        readMasterDataSocket = new NationalInstruments.Net.DataSocket(); //read from Master IAT
                        readMasterDataSocket.AutoConnect = false;
                    }
                }
                catch
                {
                    System.Windows.MessageBox.Show("National Insturments Measurement Studio 8.6 license expired, please uninstall and then reinstall.", "License Expired");    
                }
                
                //send datasocket to slave IAT
                if (isMaster)
                {
                    writeDataSocket.Data.Attributes.Add("Location", 0);
                    writeDataSocket.Data.Attributes.Add("Dive", 0);
                    writeDataSocket.Data.Attributes.Add("TimeIn", 0);
                    writeDataSocket.Data.Attributes.Add("Pilot", 0);
                    writeDataSocket.Data.Attributes.Add("Scientist", 0);
                    writeDataSocket.Data.Attributes.Add("Vehicle", 0);
                    writeDataSocket.Data.Attributes.Add("Camera", 0);
                    writeDataSocket.Data.Attributes.Add("B1E", 0);
                    writeDataSocket.Data.Attributes.Add("B1N", 0);
                    writeDataSocket.Data.Attributes.Add("B2E", 0);
                    writeDataSocket.Data.Attributes.Add("B2N", 0);
                    writeDataSocket.Data.Attributes.Add("GPSDevice", 0);
                    writeDataSocket.Data.Attributes.Add("NavDevice", 0);
                }

                if (newMissionDialog.GetNavDevice() == "DSS Southstar")
                {
                    writeDataSocket.Data.Attributes.Add("Depth", 0); //if using DSS Southstar, write PCPilot depth to NavBridge
                }

                readDataSocket.ConnectionStatusUpdated += new NationalInstruments.Net.ConnectionStatusUpdatedEventHandler(this.OnDataSocketConnectionStatusUpdated);
                readDataSocket.DataUpdated += new NationalInstruments.Net.DataUpdatedEventHandler(this.OnDataSocketDataUpdated);

                if (!isMaster)
                {
                    readMasterDataSocket.ConnectionStatusUpdated += new NationalInstruments.Net.ConnectionStatusUpdatedEventHandler(this.OnMasterDataSocketConnectionStatusUpdated);
                    readMasterDataSocket.DataUpdated += new NationalInstruments.Net.DataUpdatedEventHandler(this.OnMasterDataSocketDataUpdated);
                }

                pilotDisplay = new PilotDisplay(this);

                NavCamera.SetCameraParamters(navCameraSettings);
                NavCamera.MouseDown += new MouseButtonEventHandler(Camera_MouseDown);
                NavCamera.KeyDown += new KeyEventHandler(Camera_KeyDown);
                
                if (isMaster && !elphelConnected) //if master, connect immediately to elphel
                {
                    cameraDriver = new ElphelCameraHttpDriver(ip, annotationControl, this);
                    cameraDriver.SetDisplays(NavCamDisplay, navCameraSettings, pilotDisplay.view);
                    cameraDriver.StartThread(this.Dispatcher);
                    elphelConnected = true;
                }

                pilotDisplay.Show();
                pilotDisplay.SetPilotName(newMissionDialog.GetPilot());
                pilotDisplay.SetDiveNum(newMissionDialog.GetDiveNumber());

                PositionInputHandler.UpdatePort(ApplicationConfig.data.PositionCommConnection, ApplicationConfig.data.PositionCommBaudRate); //get nav info
                
                if (readDataSocket.IsConnected)
                {
                    readDataSocket.Disconnect();
                }

                if (!isMaster)
                {
                    readDataSocket.Connect(readTargetUrl, AccessMode.ReadAutoUpdate);
                    readMasterDataSocket.Connect(readMasterTargetUrl, AccessMode.ReadBufferedAutoUpdate);
                    dataSocketProgress = new DatasocketProgress();
                    dataSocketProgress.progressBar1.Minimum = 0;
                    dataSocketProgress.progressBar1.Maximum = 100;
                    dataSocketProgress.Show();
                    dataSocketProgress.Focus();
                }

                readDataSocket.Connect(readTargetUrl, AccessMode.ReadAutoUpdate);
                writeDataSocket.Connect(writeTargetUrl, AccessMode.Write);
            }

            private void Camera_MouseDown(object sender, MouseButtonEventArgs e)
            {
                NavCamera.Focus();
                
                if (e.RightButton == MouseButtonState.Pressed)
                {
                    NavCamera.Focus();
                }
            }

            public String ParsePosition()
            {
                string position = PositionInputHandler.PositionData;
                string parsedPosition = "N/A";
                string navDevice = newMissionDialog.GetNavDevice();

                if (position != null)   //get and parse position from serial port
                {
                    string time = "N/A";
                    string easting = "N/A";
                    string northing = "N/A";
                    string depth = "N/A";
                    string error = "N/A";
                    //string date = "N/A";

                    if (navDevice == "DSS Southstar")
                    {
                        char[] delimiterChars = { ',' };

                        string[] words = position.Split(delimiterChars);

                        if (words.Length > 4)
                        {
                            time = words[1];
                            easting = words[2];
                            northing = words[3];
                            depth = words[4];
                            error = words[5];

                            parsedPosition = "E: " + easting + ", N: " + northing + ", D: " + depth + ", E: " + error;
                        }
                        else
                        {
                            parsedPosition = "N/A";
                        }
                    }

                    else if (navDevice == "DSS Pilot")
                    {
                        parsedPosition = position;
                    }
                    else if (navDevice == "Tritech MicroNav")
                    {
                        char[] delimiterChars = { ',' };
                        string[] words = position.Split(delimiterChars);

                        if (words.Length > 11)
                        {
                            time = words[1];
                            northing = words[3] + words[2];
                            easting = words[5] + words[4];
                            //date = "N/A";
                            //parsedPosition = "0:" + words[0] + "1:" + words[1] + "2:" + words[2] + "3:" + words[3] + "4:" + words[4] + "5:" + words[5] + "6:" + words[6] + "7:" + words[7] + "8:" + words[8] + "9:" + words[9];
                            //parsedPosition = "Format: NMEA $GPRMC" + ", " + "Date: " + date + ", Time: " + time + ", LAT: " + northing + ", LON: " + easting;
                            parsedPosition = "Format: NMEA $GPGGA" + ", " + ", Time: " + time + ", LAT: " + northing + ", LON: " + easting;

                        }
                    }
                    else
                    {
                        parsedPosition = "N/A";
                    }
                }

                return parsedPosition;
            }


            private void Camera_KeyDown(object sender, KeyEventArgs e)
            {
                
                if ((DateTime.Now - lastUpdate) >= lengthOfTimeForChordStroke)
                {
                    currentKeyStack.Clear();
                }

                currentKeyStack.Add(e.Key);

                if (currentKeyStack[0] == Key.LeftCtrl) //take high res snapshot
                {
                    if (annotationControl.IsFocused)
                    {
                        string anno = annotationControl.annotationLongTextCtrl.Text;

                        annotationControl.position = ParsePosition();

                        annotationControl.SaveAnnotation(anno);
                        NavCamDisplay.Focus();
                    }
                    else
                    {
                        cameraDriver.SetSnapFull(true);
                        annotationControl.SetImageParameters(DateTime.Now);
         
                        string anno = annotationControl.annotationLongTextCtrl.Text;

                        annotationControl.position = ParsePosition();

                        annotationControl.SaveAnnotation(anno);
                        NavCamDisplay.Focus();
                    }
                }
                else if ((currentKeyStack[0] == Key.Space)) //take snapshot and start annotation
                {
                    cameraDriver.SetSnapFull(true);
                    annotationControl.SetImageParameters(DateTime.Now);
                    annotationControl.annotationLongTextCtrl.Focus();
                    annotationControl.position = ParsePosition();
                }
                else if (currentKeyStack[0] == Key.LeftShift)   //display laser rangefinder overlay
                {
                    laserOverlay = !laserOverlay;
                    LaserImage.Opacity = 0.5f;

                    if (!laserOverlay)
                    {
                        LaserImage.Opacity = 0.0f;
                    }
                }
            }

            public void TakeSnapFull()
            {
                cameraDriver.SetSnapFull(true);
                
                annotationControl.SetImageParameters(DateTime.Now);
         
                string anno = annotationControl.annotationLongTextCtrl.Text;

                annotationControl.position = ParsePosition();
                annotationControl.SaveAnnotation(anno);
                
            }

            private void Window_Closed(object sender, EventArgs e)
            {
                pilotDisplay.Close();

                if (cameraDriver != null)
                {
                    cameraDriver.EndThread();
                }
               
                readDataSocket.Disconnect();

                if (!isMaster)
                {
                    readMasterDataSocket.Disconnect();
                }

                writeDataSocket.Disconnect();
            }

            private void OnDataSocketDataUpdated(object sender, NationalInstruments.Net.DataUpdatedEventArgs e)
            {
                annotationControl.position = ParsePosition();        

                //read PCPilot data from datasocket
                if (e.Data.Attributes.Contains(depthAttributeName))
                {
                    depth = (int)e.Data.Attributes[depthAttributeName].Value;
                    pilotDisplay.SetDepth(depth);
                }
                else if (e.Data.Attributes.Contains(headingAttributeName))
                {
                    heading = (int)e.Data.Attributes[headingAttributeName].Value;
                    pilotDisplay.SetHeading(heading);
                }
                else if (e.Data.Attributes.Contains(tiltAttributeName))
                {
                    tilt = (int)e.Data.Attributes[tiltAttributeName].Value;
                    pilotDisplay.SetTilt(tilt);
                }
                else if (e.Data.Attributes.Contains(lightsAttributeName))
                {
                    light = (int)e.Data.Attributes[lightsAttributeName].Value;
                    pilotDisplay.SetLights(light);
                }
                else if (e.Data.Attributes.Contains(cameraTiltAttributeName))
                {
                    cameraTilt = (int)e.Data.Attributes[cameraTiltAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(mainTempAttributeName))
                {
                    mainTemp = (int)e.Data.Attributes[mainTempAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(camTempAttributeName))
                {
                    camTemp = (int)e.Data.Attributes[camTempAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(mainHumAttributeName))
                {
                    mainHum = (int)e.Data.Attributes[mainHumAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(camHumAttributeName))
                {
                    camHum = (int)e.Data.Attributes[camHumAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(rollAttributeName))
                {
                    roll = (int)e.Data.Attributes[rollAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(mainAftAttributeName))
                {
                    mainAft = (int)e.Data.Attributes[mainAftAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(altForwardAttributeName))
                {
                    altForward = (int)e.Data.Attributes[altForwardAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(altRearAttributeName))
                {
                    altRear = (int)e.Data.Attributes[altRearAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(horizForwardAttributeName))
                {
                    horizForward = (int)e.Data.Attributes[horizForwardAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(horizRearAttributeName))
                {
                    horizRear = (int)e.Data.Attributes[horizRearAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(forwardBackCruiseAttributeName))
                {
                    forwardBackCruise = (int)e.Data.Attributes[forwardBackCruiseAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(leftRightCruiseAttributeName))
                {
                    leftRightCruise = (int)e.Data.Attributes[leftRightCruiseAttributeName].Value;
                }
                else if (e.Data.Attributes.Contains(upDownCruiseAttributeName))
                {
                    upDownCruise = (int)e.Data.Attributes[upDownCruiseAttributeName].Value;
                }
                
                //annotationControl.SetITCP(heading, depth, cameraTilt,navCameraSettings.stillExposure,navCameraSettings.decimation); //update jpg header
                annotationControl.SetIPTC(depth, tilt, heading, light, cameraTilt, mainTemp, camTemp, mainHum, camHum, roll, forwardBackCruise, leftRightCruise, upDownCruise, mainAft, altForward, altRear, horizForward, horizRear, navCameraSettings.stillExposure, navCameraSettings.decimation); //update jpg header
                cameraDriver.SetPCPilotData(depth, tilt, heading, light, cameraTilt, mainTemp, camTemp, mainHum, camHum, roll, forwardBackCruise, leftRightCruise, upDownCruise, mainAft, altForward, altRear, horizForward, horizRear); //update CSV file

                if (newMissionDialog.GetNavDevice() == "DSS Southstar") //if using DSS Southstar, send depth to NavBridge
                {
                    writeDataSocket.Data.Attributes["Depth"].Value = depth; 
                }

                if (isMaster) //write mission parameters to datasocket
                {
                    writeDataSocket.Data.Attributes["Location"].Value = newMissionDialog.GetLocation();
                    writeDataSocket.Data.Attributes["Dive"].Value = newMissionDialog.GetDiveNumber();
                    writeDataSocket.Data.Attributes["TimeIn"].Value = newMissionDialog.GetTimeIn();
                    writeDataSocket.Data.Attributes["Pilot"].Value = newMissionDialog.GetPilot();
                    writeDataSocket.Data.Attributes["Scientist"].Value = newMissionDialog.GetScientist();
                    writeDataSocket.Data.Attributes["Vehicle"].Value = newMissionDialog.GetVehicle();
                    writeDataSocket.Data.Attributes["Camera"].Value = newMissionDialog.GetCameraIP();
                    writeDataSocket.Data.Attributes["B1E"].Value = newMissionDialog.GetB1E();
                    writeDataSocket.Data.Attributes["B1N"].Value = newMissionDialog.GetB1N();
                    writeDataSocket.Data.Attributes["B2E"].Value = newMissionDialog.GetB2E();
                    writeDataSocket.Data.Attributes["B2N"].Value = newMissionDialog.GetB2N();
                    writeDataSocket.Data.Attributes["GPSDevice"].Value = newMissionDialog.GetGPSDevice();
                    writeDataSocket.Data.Attributes["NavDevice"].Value = newMissionDialog.GetNavDevice();
                }
                writeDataSocket.Update();
            }

            private void OnDataSocketConnectionStatusUpdated(object sender, NationalInstruments.Net.ConnectionStatusUpdatedEventArgs e)
            {
                if (isMaster)
                {
                    if (readDataSocket.IsConnected)
                    {
                        MessageBox.Show("Connection established with PCPilot at " + readTargetUrl + "." , "Datasocket Connected");
                    }
                    else if (readDataSocket.IsConnected == false && dataSocketAttempted == false)
                    {
                        dataSocketAttempted = true;
                    }
                }
                else
                {
                    if (readDataSocket.IsConnected)
                    {
                        dataSocketProgress.progressBar1.Value = 75;
                        dataSocketProgress.Refresh();
                    }
                    else if (readDataSocket.IsConnected == false && dataSocketAttempted == false)
                    {
                        dataSocketAttempted = true;
                    }
                    //dataSocketProgress.Hide();
                }
            }

            private void OnMasterDataSocketDataUpdated(object sender, NationalInstruments.Net.DataUpdatedEventArgs e)
            {
                if (isMaster) //update the master datasocket mission parameters in case they are edited
                {
                    if (e.Data.Attributes.Contains(locationAttributeName))
                    {
                        newMissionDialog.SetLocation(e.Data.Attributes[locationAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(diveAttributeName))
                    {
                        newMissionDialog.SetDiveNumber(e.Data.Attributes[diveAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(timeInAttributeName))
                    {
                        newMissionDialog.SetTimeIn(e.Data.Attributes[timeInAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(pilotAttributeName))
                    {
                        newMissionDialog.SetPilot(e.Data.Attributes[pilotAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(scientistAttributeName))
                    {
                        newMissionDialog.SetScientist(e.Data.Attributes[scientistAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(vehicleAttributeName))
                    {
                        newMissionDialog.SetVehicle(e.Data.Attributes[vehicleAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(cameraAttributeName))
                    {
                        newMissionDialog.SetCameraIP(e.Data.Attributes[cameraAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(B1EAttributeName))
                    {
                        newMissionDialog.SetB1E(e.Data.Attributes[B1EAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(B1NAttributeName))
                    {
                        newMissionDialog.SetB1N(e.Data.Attributes[B1NAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(B2EAttributeName))
                    {
                        newMissionDialog.SetB2E(e.Data.Attributes[B2EAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(B2NAttributeName))
                    {
                        newMissionDialog.SetB2N(e.Data.Attributes[B2NAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(gpsDeviceAttributeName))
                    {
                        newMissionDialog.SetGPSDevice(e.Data.Attributes[gpsDeviceAttributeName].Value.ToString());
                    }
                    else if (e.Data.Attributes.Contains(navDeviceAttributeName))
                    {
                        newMissionDialog.SetNavDevice(e.Data.Attributes[navDeviceAttributeName].Value.ToString());
                    }
              
                    pilotDisplay.SetPilotName(newMissionDialog.GetPilot());
                    pilotDisplay.SetDiveNum(newMissionDialog.GetDiveNumber());
                    writeDataSocket.Update();
                }
                else //if slave
                {
                    newMissionDialog.SetLocation(e.Data.Attributes[locationAttributeName].Value.ToString());

                    string lastDiveNumber = e.Data.Attributes[diveAttributeName].Value.ToString();

                    if (lastDiveNumber != newMissionDialog.GetDiveNumber())
                    {
                        newMissionDialog.SetDiveNumber(lastDiveNumber);
                        ApplicationConfig.data.SetPath(newMissionDialog.GetDiveNumber() + " Site " + newMissionDialog.GetLocation() + "2010" + "\\");
                        ApplicationConfig.ValidateAndCreateDirectories();
                    }
                    else
                    {
                        newMissionDialog.SetDiveNumber(lastDiveNumber);
                    }

                    newMissionDialog.SetTimeIn(e.Data.Attributes[timeInAttributeName].Value.ToString());
                    newMissionDialog.SetPilot(e.Data.Attributes[pilotAttributeName].Value.ToString());
                    newMissionDialog.SetScientist(e.Data.Attributes[scientistAttributeName].Value.ToString());
                    newMissionDialog.SetVehicle(e.Data.Attributes[vehicleAttributeName].Value.ToString());
                    newMissionDialog.SetCameraIP(e.Data.Attributes[cameraAttributeName].Value.ToString());
                    newMissionDialog.SetB1E(e.Data.Attributes[B1EAttributeName].Value.ToString());
                    newMissionDialog.SetB1N(e.Data.Attributes[B1NAttributeName].Value.ToString());
                    newMissionDialog.SetB2E(e.Data.Attributes[B2EAttributeName].Value.ToString());
                    newMissionDialog.SetB2N(e.Data.Attributes[B2NAttributeName].Value.ToString());
                    newMissionDialog.SetGPSDevice(e.Data.Attributes[gpsDeviceAttributeName].Value.ToString());
                    newMissionDialog.SetNavDevice(e.Data.Attributes[navDeviceAttributeName].Value.ToString());

                    pilotDisplay.SetPilotName(newMissionDialog.GetPilot());
                    pilotDisplay.SetDiveNum(newMissionDialog.GetDiveNumber());

                    if (newMissionDialog.GetCameraIP() == "192.168.2.212") //if downward camera on master, forward camera on slave
                    {
                        ip = "192.168.2.213";
                    }
                    else if (newMissionDialog.GetCameraIP() == "192.168.2.213") //if forward camera on master, downward camera on slave
                    {
                        ip = "192.168.2.212";
                    }
                    else if (newMissionDialog.GetCameraIP() == "192.168.2.210")
                    {
                        ip = "192.168.2.211";
                    }
                    else if (newMissionDialog.GetCameraIP() == "192.168.2.211")
                    {
                        ip = "192.168.2.210";
                    }
                    else if (newMissionDialog.GetCameraIP() == "192.168.2.217")
                    {
                        ip = "192.168.2.216";
                    }

                    if (!elphelConnected) //once slave has master elphel ip, connect to elphel and finish progress bar
                    {
                        cameraDriver = new ElphelCameraHttpDriver(ip, annotationControl, this);
                        cameraDriver.SetDisplays(NavCamDisplay, navCameraSettings, pilotDisplay.view);
                        cameraDriver.StartThread(this.Dispatcher);
                        elphelConnected = true;
                        dataSocketProgress.progressBar1.Value = 100;
                        dataSocketProgress.Refresh();
                        dataSocketProgress.Hide();
                    }
                }
            }

            private void OnMasterDataSocketConnectionStatusUpdated(object sender, NationalInstruments.Net.ConnectionStatusUpdatedEventArgs e)
            {
                if (!isMaster) //if slave, see if connected to Master datasocket
                {
                    if (readMasterDataSocket.IsConnected)
                    {
                        MessageBox.Show("Connection established with Master IAT at " + readMasterTargetUrl + ".", "Datasocket Connected");
                    }
                    else if (readMasterDataSocket.IsConnected == false && dataSocketAttempted == false)
                    {
                        dataSocketAttempted = true;
                    }
                }
            }

            private void annotationControl_Loaded(object sender, RoutedEventArgs e)
            {
            }

            public int getAnnotationSerial()
            {
                return annotationSerial;
            }

            public void incrementAnnotatioSerial()
            {
                annotationSerial++;
            }

            private void EditMissionButton_Click(object sender, RoutedEventArgs e)
            {
                newMissionDialog.ShowDialog() ;

                pilotDisplay.SetPilotName(newMissionDialog.GetPilot());
                pilotDisplay.SetDiveNum(newMissionDialog.GetDiveNumber());
                ApplicationConfig.data.SetPath(newMissionDialog.GetDiveNumber() + " Site " + newMissionDialog.GetLocation() + "2010" + "\\");
                ApplicationConfig.ValidateAndCreateDirectories();
            }

            private void Window_Loaded(object sender, RoutedEventArgs e)
            {

            }

        }
    }
