﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImageAnnotationTool
{
    static class PositionInputHandler
    {
        static public DSS.Communication.Serial.Port port;
        static public DateTime lastCommTime;

        static public int PortnameToID(string portname)
        {
            int val = 0;
            try
            {
                val = Int32.Parse(portname.Trim(new char[] { 'C', 'O', 'M' }));
            }
            catch (FormatException)
            {
            }
            return val;
        }

        static PositionInputHandler()
        {
            port = new DSS.Communication.Serial.Port();
            port.RxData += new DSS.Communication.Serial.Port.RXDataHandler(port_RxData);
        }

        static string serialDataStr;
        static string _positionData = "N/A";
        static int state = 0;
        static void port_RxData(object sender, EventArgs e)
        {
            DSS.Communication.Serial.Port.RXEventArgs ea = (DSS.Communication.Serial.Port.RXEventArgs) e;
            ASCIIEncoding encoding = new ASCIIEncoding();
            foreach (char c in ea.buf)
            {
                if (state == 0)
                    if (c == 'M' || c == '$') {//M is start of line for plats std string
                        state = 1;
                        serialDataStr = "";
                    }
                if (state == 1) {
                    if (c == '\n' || c=='\r')
                        state = 2;
                    else
                    if (serialDataStr.Length > 256)
                        state = 0;
                    else
                        serialDataStr += c;
                }
                if (state == 2)
                {
                    PositionData = serialDataStr;
                    state = 0;
                }
            }
            lastCommTime = DateTime.Now;
        }

        static public string PositionData
        {
            set
            {
                lock (_positionData)
                {
                    _positionData = value;
                }
            }

            get
            {
                string r;
                if (_positionData == null)
                    return null;
                lock (_positionData)
                {
                    r = _positionData;
                }
                return r;
            }
        }

        static public void UpdatePort(string PortName, int BaudRate)
        {
            ApplicationConfig.data.PositionCommBaudRate = BaudRate;
            ApplicationConfig.data.PositionCommConnection = PortName;
            lock (port) {
                port.Close();
                if (BaudRate != 0)
                    port.speed = BaudRate;
                if (PortName != null)
                    port.id = PortnameToID(PortName);
                port.Open();
                port.EnableRxThread();
            }

            ApplicationConfig.Save();
            return;
        }

    }
}
