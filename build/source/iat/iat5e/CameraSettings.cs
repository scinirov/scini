﻿/*
CameraSettings.cs

Copyright (C) <2010>  <Moss Landing Marine Laboratories>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ImageAnnotationTool
{
    [Serializable()]
    [XmlRoot("ImageAnnotationTool_Camera_Settings")]
    public class CameraSettings
    {
        public int streamExposure = 30; 
        public int stillExposure = 30;
        public int decimation = 4;
        public int jpegQuality = 90;
        public int delayMS = 1000;
        public int bin = 1;
        public double gamma = 1;
        public double saturation = 1;

        public bool dirtyFlag = false;
        public bool snapFull = false;
        public bool rawMode = false;
        public bool autoSave = false;
        public bool streamAuto = false;
    }
}
